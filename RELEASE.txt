Version-0.9:
+Support for internal static resources (such as html,css,js files)
+Support for external static resources
M New Build File (including demo with internal resources)
+ Added Core servlet (supports doStatic and reply methods) 
M All demo servlets migrated to CoreServlets
-UTF Servlet (all functions were moved to CoreServlet)

Version-0.8:
+Config file support
+Argument validators
+New Server logging

Version-0.7.1:
+Added support for code 405(Method not implemented) as filed in RFC

Version-0.7:
+New Kernel (Core handlers added)
+Error Pages

Version-0.6:
+ Connectors
+ Support for many Handler
M Updated demo
M Internal Structure
- All methods in server and app
