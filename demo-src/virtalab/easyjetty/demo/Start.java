package virtalab.easyjetty.demo;

import java.util.Scanner;

import virtalab.easyjetty.demo.servers.CompleteWebServerDemo;
import virtalab.easyjetty.demo.servers.WebServerDemo;
import virtalab.easyjetty.demo.servers.WebServerDemoWithArgs;
import virtalab.sys.Shell;


public class Start {

	private static Scanner scanner;

	/**
	 * Demo begins here
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {

		//This way you can overwrite log level (and other settings) in runtime
		//this is recommended for debug during development purposes only
		//leaving this setting in production is bad idea,
		//because system at server simply cannot overwrite this setting
		System.setProperty("virtalab.LEVEL","INFO");

		scanner = new Scanner(System.in);
		//we using numbers just to keep code valid in Java 6
		int type = 0;
		int debug = 0;
		int debugCore = 0;
		//flags
		boolean isValidMode = false;
		boolean isValidDebugValue = false;
		boolean isValidCoreDebugValue = false;

		//////////////////////////////////////////////////////////////////////////
		//Let's rock!
		Shell.echo("Hello and Welcome to EasyJetty!");


		//server type
		do{
		Shell.echo("Select server mode you want to run:");
		Shell.echo("1. WebServer (no args)");
		Shell.echo("2. WebServer with Arguments");
		Shell.echo("3. Complete Configuration Web Server example (Root required to start) ");

		String serverType = scanner.nextLine();

		//Scanner returns null when input was interrupted (Ctrl+С pressed for example).
		if(serverType==null){ return; }

		try{
			type = Integer.parseInt(serverType);
			//Server type
			switch (type) {
			case 1:
			case 2:
			case 3:
				isValidMode =true;
				break;
			default:
				Shell.eecho("Unknown option. Only options written above are valid. Try again");
				isValidMode = false;
				break;
			}
		}catch(NumberFormatException e){
			Shell.eecho("Unknown option. Only options written above are valid. Try again");
			isValidMode = false;
		}
		}while(!isValidMode);


		//Also in this demo we will give user to choose enable debug info or not
		do{
		Shell.echo("Use debug?");
		Shell.echo("0. No | 1. Yes");
		String debugOpt = scanner.nextLine();

		try{
			debug = Integer.parseInt(debugOpt);
			//setting debug first (before launching servers)
			switch (debug) {
			case 0:
				System.setProperty("virtalab.LEVEL","INFO");
				isValidDebugValue = true;
				break;
			case 1:
				System.setProperty("virtalab.LEVEL","DEBUG");
				isValidDebugValue = true;
				break;
			default:
				Shell.eecho("Unknown option. Only 0 or 1 are valid. Try again.");
				isValidDebugValue = false;
				break;
			}
		}catch(NumberFormatException e){
			Shell.eecho("Unknown option. Only 0 or 1 are valid. Try again.");
			isValidDebugValue = false;
		}
		}while(!isValidDebugValue);


		//Debug Core
		do{
		Shell.echo("Debug core?");
		Shell.echo("0. No | 1. Yes");

		String debugCoreIn = scanner.nextLine();

		try{
			debugCore = Integer.parseInt(debugCoreIn);
			//debug core
			switch (debugCore) {
			case 0:
				System.setProperty("org.eclipse.jetty.LEVEL","INFO");
				isValidCoreDebugValue = true;
				break;
			case 1:
				System.setProperty("org.eclipse.jetty.LEVEL","DEBUG");
				isValidCoreDebugValue = true;
				break;
			default:
				Shell.eecho("Unknown option. Only 0 or 1 are valid. Try again.");
				break;
			}
		}catch(NumberFormatException e){
			Shell.eecho("Unknown option. Only 0 or 1 are valid. Try again.");
			isValidCoreDebugValue = false;
		}

		}while(!isValidCoreDebugValue);

		//closing scanner
		scanner.close();

		//Starting Server
		switch (type){
		case 1:
			WebServerDemo.start();
			break;
		case 2:
			WebServerDemoWithArgs.start(args);
			break;
		case 3:
			CompleteWebServerDemo.start(args);
			break;
		default:
			break;
		}


	}


}
