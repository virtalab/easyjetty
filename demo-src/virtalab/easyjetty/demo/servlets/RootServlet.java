package virtalab.easyjetty.demo.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.core.servlets.CoreServlet;

@SuppressWarnings("unused")
public class RootServlet extends CoreServlet {

	private static final long serialVersionUID = 68066920756599663L;
	private static final Logger log = Log.getLogger(RootServlet.class);
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{

		//As for me this is good practice for catch-all (root) Servlet

		if(req.getRequestURI().endsWith(".html")){
			//working with resource
			String resource = doStatic(req,resp);
			if(resource == null){
				return;
			} else {
				reply(resource, resp);
				return;
			}
		}
		//PAGE (this is very bad style to generate HTML from Java as for me. This one is just example for Servlet contains)
		StringBuffer sb = new StringBuffer();

		sb.append("<h1>Jet Server</h1>");
		sb.append("If you able to read this. This means that you added WebServer to app successfully.");
		sb.append("<h2>Visit here for demo</h2>");
		sb.append("<ul>");
		sb.append("<li><a href=\"debug\">DebugServlet</a> with some debug info</li>");
		sb.append("<li><a href=\"lang\">LangServlet</a> with i18n example (use ?hl=ru (for Russian))</li>");
		sb.append("</ul>");
		//Reply
		resp.addHeader("X-Powered-By","Java, EasyJetty and RootServlet");

		reply(sb.toString(),resp);

	}
}
