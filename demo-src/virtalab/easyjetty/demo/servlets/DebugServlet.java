package virtalab.easyjetty.demo.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virtalab.easyjetty.core.servlets.CoreServlet;

public class DebugServlet extends CoreServlet {

	private static final long serialVersionUID = 68066920756599663L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{

		//VARs
		String localAddr = req.getLocalAddr();

		String remoteIP = req.getRemoteAddr();
		String remoteHost = req.getRemoteHost();

		//PAGE (this is very bad style to generate HTML from Java as for me. This one is just example for Servlet contains)
		StringBuffer sb = new StringBuffer();

		sb.append("<h1>Debug</h1>");
		sb.append("<h2>Local side (WebServer) </h2>");
		sb.append("WebServer runs at: "+localAddr+"<BR>");

		sb.append("<h2>Remote side (browser)</h2>");
		sb.append("Remote IP: "+remoteIP+"<BR>");
		sb.append("Remote Host: "+remoteHost+"<BR>");

		//Reply
		resp.addHeader("SuperHeader","Java Rocks");

		reply(sb.toString(),resp);
	}
}
