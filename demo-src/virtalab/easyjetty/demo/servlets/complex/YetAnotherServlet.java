package virtalab.easyjetty.demo.servlets.complex;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virtalab.easyjetty.core.servlets.CoreServlet;

public class YetAnotherServlet extends CoreServlet {

	private static final long serialVersionUID = 68066920756599663L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{

		//PAGE (this is very bad style to generate HTML from Java as for me. This one is just example for Servlet contains)
		StringBuffer sb = new StringBuffer();

		sb.append("This is just yet another servlet to provide an example how to set EasyJetty.");

		//Reply
		reply(sb.toString(),resp);
	}
}
