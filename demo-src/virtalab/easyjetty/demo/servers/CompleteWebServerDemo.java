package virtalab.easyjetty.demo.servers;

import virtalab.easyjetty.WebServer;
import virtalab.easyjetty.app.exceptions.WebServerException;
import virtalab.easyjetty.core.loggers.AccessLogger;
import virtalab.easyjetty.core.loggers.ErrorLogger;
import virtalab.easyjetty.demo.servlets.YksiHandler;
import virtalab.sys.Shell;

public class CompleteWebServerDemo {
	public static void start(String[] args){
		//creating new server
		try{
			WebServer server = new WebServer(args);

			//We hardcoding host and port in this example
			//NOTE: those values cannot be overriden in config or by args
			server.setHost("localhost");
			server.setPort(80);

			//Also let's setup logging
			/*
			if logging settings are present in config (and valid),
			method getAccessLog will create logger to file defined in config
			*/

			AccessLogger accessLog = server.getAccessLogger();
			//this filename will overwrite setting in config file
			//this file will be created in the same folder as demo jar
			accessLog.setFilename("access.log");
			accessLog.setAppend(true);
			accessLog.setLogTimeZone("MSK");
			//adding access logger
			server.addLogger(accessLog);

			//Ok. Let's do the same for error log
			ErrorLogger errorLog = server.getErrorLogger();
			errorLog.setFilename("error.log");
			errorLog.setAppend(true);
			errorLog.setLogTimeZone("MSK");
			//adding
			server.addLogger(errorLog);

			/*
			  Let's set offset from /
			  in this example only requests to http://localhost/site/ will be served by us
			  this useful when you have Nginx in front of EasyJetty
			  Again: hardcoding is possible here, but better use config value server.offset
			 */
			server.setSiteBase("/site");

			//by default be already have RootHandler (which serves all requests to http://localhost/site/)

			//telling to RootHandler where it can find servlets
			server.setServletPackage("virtalab.easyjetty.demo.servlets");
			//we are ready to add servlet
			server.addServlet("RootServlet","/");
			server.addServlet("DebugServlet","/debug");
			server.addServlet("LangServlet","/lang");

			//thats all for RootHandler, we are commiting it
			server.commitRootHandler();

			//Also you can add Jetty Native Servlets
			//YksiHandler extends AbstractHandler
			YksiHandler yksi = new YksiHandler();
			server.addHandler(yksi, "/yksi");

			//After this let's create our custom handler and name it "CustomHandler"
			server.createHandler("CustomHandler","/my","virtalab.easyjetty.demo.servlets.complex");
			//as you can see we set base to "/my" this means we can access this handler's servlets at path http://localhost/site/my

			//adding servlet
			server.addServlet("MyCustomServlet","/");
			server.addServlet("YetAnotherServlet","/yet"); //this servlet can be access from http://localhost/site/my/yet

			//do not forget to commit handler
			server.commitHandler();

			//Ok. Let's check how it works!
			server.run();

		}catch(WebServerException e){
			//We do throw exceptions, as we pretend to be embbeded solution within your application
			Shell.eecho("Could not start web server due to following error: "+e.getMessage());
		}
	}
}
