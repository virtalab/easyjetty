package virtalab.easyjetty.demo.servers;

import virtalab.easyjetty.WebServer;
import virtalab.easyjetty.app.exceptions.WebServerException;
import virtalab.sys.Shell;

public class WebServerDemoWithArgs {
	public static void start(String[] args){
		//creating new server
		try{
			WebServer server = new WebServer(args);

			//first telling server where it can find servlets
			server.setServletPackage("virtalab.easyjetty.demo.servlets");
			//we are ready to add servlets
			server.addServlet("RootServlet","/");
			server.addServlet("DebugServlet","/debug");
			server.addServlet("LangServlet","/lang");

			//ready for run. Note: If server starts - no code is reachable after next line.
			server.run();

			//Optional Settings
			//optionally you can set offset from /
			//server.setSiteBase("/site");

			//You can set host and port, before run
			//NOTE: This is not best way to so. Better use values in web.config, instead of hardcodding them here
			//But final solution is up to you.
			//easyServer.setHost("127.0.0.1");
			//easyServer.setPort(9600);

		}catch(WebServerException e){
			//We do throw exceptions, as we pretend to be embbeded solution within your application
			Shell.eecho("Could not start web server due to following error: "+e.getMessage());
		}
	}
}
