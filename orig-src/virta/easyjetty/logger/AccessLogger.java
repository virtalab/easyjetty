package virta.easyjetty.logger;

import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;

public final class AccessLogger extends NCSARequestLog {
	public AccessLogger(){
		super();
	}
	public AccessLogger(final String filename){
		super(filename);
	}
	
	@Override
	public void log(final Request request, final Response response){
		super.log(request, response);
	}
}
