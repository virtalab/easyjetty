package virta.easyjetty.handlers;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import virta.easyjetty.struct.ErrorPageStruct;

public class ErrorHandler extends AbstractHandler {

	private static ErrorHandler self;
	
	private HashMap<Integer, ErrorPageStruct> values = new HashMap<Integer,ErrorPageStruct>();
	
	public static ErrorHandler getHandler(){
		if(self==null){
			self=new ErrorHandler();
		}
		return self;
	}
	
	private ErrorHandler(){
		this.fill();
	}
	
	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response){
		
		// TODO Allow customize error pages using Settings
		int code = response.getStatus();
		if(values.containsKey(code)){
			String shortDesc = values.get(code).getShortDesc();
			String longDesc =  values.get(code).getLongDesc();
			String webDesc =   values.get(code).getWebDesc();
			String errPage="";				
			errPage = "<html>" +
					  "<head>" +
  					  "<title>"+ code + " - "+shortDesc +" ("+webDesc+")</title>" +
					  "</head>" +
					  "<body bgcolor=\"navy\">" +
					  "<h1 align=\"center\" style=\"color: white; font-size: 100pt  \">:( </h1>" +
					  "<h1 align=\"center\" style=\"color: white; font-size: 50pt  \">"+shortDesc +" ("+webDesc+")</h1>" +
					  "<h2 align=\"center\" style=\"color: white; font-size: 30pt  \">"+longDesc+"</h2>"+
					  "</body>" +
					  "</html>";
			try{
			response.getWriter().println(errPage);
			}catch(Exception e){
				response.setStatus(500);
			}
			baseRequest.setHandled(true);
		}
	}
	/**
	 * Fills HashMap with values
	 */
	private void fill(){
		
		
		//400
		ErrorPageStruct struct = new ErrorPageStruct();
		struct.setWebDesc("Bad Request");
		struct.setShortDesc("Got not valid request");
		struct.setLongDesc("Request to page was not valid");
		values.put(400, struct);
		
		//403
		struct = new ErrorPageStruct();
		struct.setWebDesc("Forbidden");
		struct.setShortDesc("Resource unavailable");
		struct.setLongDesc("Access to requested request is denied by server");
		values.put(403, struct);
		
		//404
		struct = new ErrorPageStruct();
		struct.setWebDesc("Not Found");
		struct.setShortDesc("Web Page not found");
		struct.setLongDesc("No such file or directory at server");
		values.put(404, struct);
		
		//405
		struct = new ErrorPageStruct();
		struct.setWebDesc("Method Not Allowed");
		struct.setShortDesc("Method not applicable for resource");
		struct.setLongDesc("This method is not applicable for requested resource. Serlvet doesn't support this method.");
		values.put(405, struct);
		
		//500
		struct = new ErrorPageStruct();
		struct.setWebDesc("Internal Server Error");
		struct.setShortDesc("Server Error");
		struct.setLongDesc("Server got unhandled Exception or Error");
		values.put(500, struct);
		
		//501
		struct = new ErrorPageStruct();
		struct.setWebDesc("Not implemented");
		struct.setShortDesc("Method not implemented");
		struct.setLongDesc("This method is not implemented at server");
		values.put(501, struct);
	}
}