package virta.easyjetty.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import virta.sys.Shell;
/**
 * Debug Handler
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class YksiHandler extends AbstractHandler {

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
			Shell.eecho("Reply from YksiHandler");
			Shell.eecho("I got target "+target);
			Shell.eecho("Request method is: "+ request.getMethod());
			Shell.eecho("End of Reply from YksiHandler");
			
			baseRequest.setHandled(true);
			response.getWriter().println("Yksi");
	}

}
