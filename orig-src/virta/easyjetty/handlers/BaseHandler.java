package virta.easyjetty.handlers;

import org.eclipse.jetty.server.handler.AbstractHandler;

public abstract class BaseHandler extends AbstractHandler {
	protected String base;

	public String getBase() {
		return base;
	}
	
}
