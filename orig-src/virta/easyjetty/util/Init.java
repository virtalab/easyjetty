package virta.easyjetty.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virta.easyjetty.handlers.RootHandler;
import virta.easyjetty.keepers.App;
import virta.easyjetty.keepers.ServerConfig;
import virta.regexp.RegExps;

public class Init {
	//static field
	private static final Logger log = Log.getLogger(Init.class);
	private static final Init init = new Init();
	private static final String lt = System.getProperty("line.separator");

	//constructor
	private Init(){
		App.init();
		ServerConfig.init();
	}
	
	//get Instance
	
	
	//methods
	public static void init(String[] args) throws ExecutionException, IOException{
		
		//Analyze of args
		log.info("Analyzing arguments");
		int size = args.length;
		
		if(size<1){
			//no args - no action
			log.warn(init.showUsageError(size));
		}
		if(size>=1){
			//1+ arg
			//if this config, port, host or some not valid shit ?
			if(init.isConfig(args[0])){
				//validate
				if(Validator.isValidConfig(args[0])){
					//import values to System.properties
					try{
						init.importProperties(args[0]);
					}catch(FileNotFoundException e){
						log.warn(args[0]+ ": no such file");
						log.warn(e);
						throw new ExecutionException(e);
					}catch(IOException e){
						log.warn(e);
						throw new ExecutionException(e);
					}
				} else {
					//not valid config
					log.warn("Config file "+args[0]+" was not found or not readable.");
					throw new IOException();
				}
			} else if(init.isPort(args[0])) {
				//validate
				if(Validator.isValidPort(args[0])){
					//set port to server config
					int port = Integer.parseInt(args[0]);
					ServerConfig.getConfig().setPort(port);
				} else {
					//port not valid: report and set default 
					log.info("Setting default port");
					int port = 0;
					ServerConfig.getConfig().setPort(port);
				}
			} else if(init.isIpAddr(args[0])){
				//validate
				if(Validator.isValidIpAddr(args[0])){
					ServerConfig.getConfig().setHost(args[0]);
				} else {
					log.warn("Given Host " + args[0] + " cannot be assigned to server.This Host doesn't match any of your machine ip addresses");
					log.warn("Use default listen at all interfaces host 0.0.0.0");
					ServerConfig.getConfig().setHost("0.0.0.0");
				}
			} else {
				//none of above
				log.warn(init.showUsageError());
			}
		}
		
		if(size>=2){
			//args[1]
			if(init.isPort(args[1])) {
				//validate
				if(Validator.isValidPort(args[1])){
					//set port to server config
					int port = Integer.parseInt(args[1]);
					ServerConfig.getConfig().setPort(port);
				} else {
					//port not valid: report and set default 
					log.info("Setting default port");
					int port = 0;
					ServerConfig.getConfig().setPort(port);
				}
			} else if(init.isIpAddr(args[1])) {
				//validate
				if(Validator.isValidIpAddr(args[1])){
					ServerConfig.getConfig().setHost(args[1]);
				} else {
					log.warn("Given Host " + args[1] + " cannot be assigned to server.This Host doesn't match any of your machine ip addresses");
					log.warn("Use default host 0.0.0.0");
					ServerConfig.getConfig().setHost("0.0.0.0");
				}
			} else {
				//arg[2] is not valid
				log.warn("Argument "+args[1]+" appears to be neighter valid host or port. Not using this argument"+lt+init.showUsage());
			}	
		}
		
		//setting values
		 //host
		 String host = init.getProperty("server.host","");
		 if(!host.equals("")){
			 log.info("Setting host");
			 if(Validator.isValidIpAddr(host)){
				 ServerConfig.getConfig().setHost(host);
			 } else {
				 log.warn("server.host is not valid. Skipping and use default host 0.0.0.0");
			 }
		 }
		 
		 //port
		 String port = init.getProperty("server.port","");
		if(!port.equals("")){
			log.info("Setting port");
			if(Validator.isValidPort(port)){
				int portInt = Integer.parseInt(port);
				ServerConfig.getConfig().setPort(portInt);
			} else {
				log.warn("server.port is not valid. Skipping and using custom port");
			}
		}
		
		//access.log
		String accessLog = init.getProperty("access.log","");
		if(!accessLog.equals("")){
			log.info("Setting access log");
			//check on write
			if(Validator.isValidLogFile(accessLog)){
				//pass value		
				App.getApp().setAccessLog(LoggerTuner.setupAccessLogger(accessLog));
				//log.debug("MAYDAY "+App.getApp().getAccessLog().toString());
			} else {
				//report
				log.warn(accessLog+" is not writable or doesn't exists. Will not log requests");
			}
		}

		//error.log
		String errorLog = init.getProperty("error.log","");
		if(!errorLog.equals("")){
			log.info("Setting error log");
			//check on write
			if(Validator.isValidLogFile(errorLog)){
				//pass value
				App.getApp().setErrorLog(LoggerTuner.setupErrorLogger(errorLog));
			} else {
				log.warn(errorLog+" is not writable or doesn't exists. Will not log error requests");
			}
		}
		
		//pidFile
		String pidFile = init.getProperty("pid.file","");
		log.info("Setting pid file");
		if(!pidFile.equals("")){
			//check on write
			if(Validator.isValidLogFile(pidFile)){
				//pass value
				App.getApp().setPidFile(pidFile);
			} else {
				log.warn("Pid file is not writable or cannot be created.");
			}		
		}
		
		//Server Token
		String serverToken = init.getProperty("server.token","");
		if(!serverToken.equals("")){
			log.info("Setting Server Token");
			if(Validator.isValidServerToken(serverToken)){
				App.getApp().setServerToken(serverToken);
			} else {
				//default value
				log.warn("Server Token is not valid. Using default.");
				App.getApp().setServerToken("PROD");
			}
		}
		//Server Prod
		String serverProd = init.getProperty("server.prod","");
		if(!serverProd.equals("")){
			log.info("Setting Custom Server Product");
			App.getApp().setServerProduct(serverProd);
		}
		
		//Server version
		String serverVer = init.getProperty("server.version","");
		if(!serverVer.equals("")){
			log.info("Setting Custom Version");
			App.getApp().setServerVersion(serverVer);
		}
		//TODO static.enabled = on/off
		//Resources Location
		String resLoc = init.getProperty("static.location", "");
		if(!resLoc.equals("")){
			if(!Validator.isValidStaticLocation(resLoc)){
				//not valid ->  set default
				System.setProperty("static.location","in");
			}
		} else {
			//default
			System.setProperty("static.location","none");
		}
		
		//Resource root
		String resRoot = init.getProperty("static.root","");
		if(!resRoot.equals("")){
		if(!Validator.validateResourceRoot()){
			log.warn("static.root is not valid. So you cannot use static resources. You will always get error 500.  Use debug mode for more info");
			//debug info
			String code = System.getProperty("static.code","unknown");
			log.debug("Error code: "+code);
		}
		} else {
			//static.root not set
			//resources location is set... we need to warn user about site root
			if(!resLoc.equals("none")){
				log.warn("static.root is not set. So you cannot use static resources. You will always get error 500.");
			}
			//if location set to "in" - reset to default
			if(resLoc.equals("in")){
				System.setProperty("static.root","/");
			}
		}
		
		//TODO do we need this?
		log.info("Init root handler");		
		RootHandler.getInstance();
		
		log.info("Server init is done");
	}
	
	/**
	 * Tests if arg is config
	 * 
	 * @param configStr arg
	 * @return true-if config string is about to be config, false  -if not
	 */
	public boolean isConfig(String configStr){
		return configStr.endsWith(".conf");
	}
	
	/**
	 * Checks if arg  is port
	 * @param portStr arg
	 * @return true - when value can be parsed to int, false - elsewhere
	 */
	public boolean isPort(String portStr){
		try{
			Integer.parseInt(portStr);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	/**
	 * checks if host is appears to be an IP address
	 * @param ipStr string with IP Address
	 * @return true - if host appears to be IP address (version 4)
	 */
	public boolean isIpAddr(String ipStr){
		return RegExps.IPv4(ipStr);
	}
	
	public String showUsageError(){
		return this.showUsageError(0);
	}
	public String showUsageError(int argSize){
		StringBuffer sb = new StringBuffer();
		
		String error = "Wrong number of arguments were send. Expected: 1 or 2. Got: "+argSize;
		String aftermath = "Server will start with default values.";
		
		if(argSize!=0){ sb.append(error +lt); }

		sb.append(this.showUsage());
		sb.append(aftermath);
		
		return sb.toString();
	}			
	public String showUsage(){
		StringBuffer sb = new StringBuffer();
	
		String vuStr = "Valid usages are:";
		String usage0 = "Usage: java -D<valid-option>=<value> -jar easyjetty.jar <config>";
		String usage1 = "Usage: java -D<valid-option>=<value> -jar easyjetty.jar <port> <host>";
		String usage2 = "Usage: java -D<valid-option>=<value> -jar easyjetty.jar <host> <port>";
		
		sb.append(vuStr + lt);
		sb.append(usage0 +lt);
		sb.append(usage1 +lt);
		sb.append(usage2 +lt);
		
		return sb.toString();
	}
	
	public void importProperties(String fileName) throws FileNotFoundException, IOException{
		Properties props = new Properties();
		props.load(new FileInputStream(fileName));
		
		Enumeration<Object> e = props.keys();
		
		while(e.hasMoreElements()){
			String key = (String) e.nextElement();
			String value = props.getProperty(key);
			//add to System props
			System.setProperty(key, value);
		}
	}
		
	private String getProperty(String key,String def){
		String prop = System.getProperty(key,def);
		prop = prop.trim();
		return prop;
	}
	

}
