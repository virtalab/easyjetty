package virta.easyjetty.util;
/**
 * Static constants
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class S {
	/**
	 * Minimal available port
	 */
	public static final int MIN_PORT=1;
	/**
	 * Maximum available port (as defined in TCP/IP)
	 */
	public static final int MAX_PORT=65535;
	/**
	 * Ports before Root Port cannot be assigned for non-Root user
	 */
	public static final int ROOT_PORT=1024;
	/**
	 * Version
	 */
	public static final String VERSION="0.8";
	/**
	 * Product
	 */
	public static final String PRODUCT="Easyjetty";
}
