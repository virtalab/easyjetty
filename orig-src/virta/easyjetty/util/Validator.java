package virta.easyjetty.util;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;

import javax.naming.ConfigurationException;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virta.sys.Env;

public class Validator {
	private static final Logger log = Log.getLogger(Validator.class);
	public static void check(String[] args) throws ConfigurationException,IndexOutOfBoundsException, NumberFormatException{
		if(args==null){
			throw new ConfigurationException("No arguments were supplied. ");
			
		}
		
		if(args.length<1){
			String lineTerminator = System.getProperty("line.separator");
			String message =  
			 "Wrong number of arguments were send. Expected: 1 or 2. Got: "+args.length + lineTerminator +
			 "Usage: java -jar -D<valid-option>=<value> <port> <host>";
			throw new IndexOutOfBoundsException(message);
		}
		//checking port
		String rawPort = args[0];
		int intPort = Integer.parseInt(rawPort);
		if(intPort < S.MIN_PORT || intPort > S.MAX_PORT){
			throw new NumberFormatException("Server Port must be a Number from 1 to 65534");
		}
		//TODO make this System-independent
		if(Env.getMyOS()=="unix"){
		if(intPort <= S.ROOT_PORT){
			//check for root
			
			String user = System.getProperty("user.name");
			if(!user.equals("root")){
				throw new NumberFormatException("You must be root to start server at ports less then 1024");
			}
		}
		}
	}
	/**
	 * Checks if config file exist and readable
	 * @param filename
	 * @return true - when readable, false - elsewhere
	 */
	 public static boolean isValidConfig(String filename){
		File file = new File(filename);
		return (file.isFile() && file.canRead());
	 }
	 /**
	  * String overload for port validator
	  * @param portStr string with port number
	  * @return true if port can be assigned to server, else false
	  */
	 public static boolean isValidPort(String portStr){
		 int port = Integer.parseInt(portStr);
		 return Validator.isValidPort(port);
	 }
	/**
	 * Checks if port is in valid area
	 * 
	 * @param port port number
	 * @return true if port can be assigned to server, else false
	 */
	public static boolean isValidPort(int port){
		if(port < S.MIN_PORT || port > S.MAX_PORT){
			log.warn("Server Port must be a Number from 1 to 65534");
			return false;
		}
		
		//TODO make this System-independent
		try{
			String OS = Env.getMyOS();
			if(OS!=null){
				if(OS=="unix"){
					if(port <= S.ROOT_PORT){
					//check for root
					String user = System.getProperty("user.name");
					if(!user.equals("root")){
						log.warn("You must be root to start server at ports less then 1024");
						return false;
					}
				}
			}
			}
		}catch(NullPointerException npe){
			log.warn("Your OS cannot be recognized. Skipping root port check.");
		}
		//if we still alive, then all checks passed
		return true;
		
	}
	/**
	 * Check if IP address can be used at this system
	 *  
	 * @param ip string with IP address
	 * @return true - if IP is valid and can be used to bind this server to, false - elsewhere  
	 * @throws SocketException when failed to scan system for interfaces (for example: not enough rights)
	 */
	public static boolean isValidIpAddr(String ip) throws SocketException{
		 ArrayList<String> ipList = Env.getMyIps("ipv4");
		 for (String ipAddr: ipList){
			 if(ipAddr.equals(ip)){
				 return true;
			 }
		 }
		 //if we are here - no match found, returning false
		 return false;
	 }

	/**
	 * Check if supplied log file Resource is writable, if file don't exist it will be created, if dir is writable
	 * 
	 * @param fileName Name of Resource
	 * @return True when file is writable or can be created, false - elsewhere
	 */
	public static boolean isValidLogFile(String fileName){
		File file = new File(fileName);
		if(file.isFile()){
			//file exists, check on write
			return file.canWrite();
		} else {
			//not exists, trying to create
			try {
				file.createNewFile();
			} catch (IOException e) {
				//cannot create
				return false;
			}
		}
		//if we reached this, that means we created file
		return true;
	}
	/**
	 *Checks if Server token is one of valid values
	 * 
	 * @param token Full,Prod or Off header mode
	 * @return check result
	 */
	public static boolean isValidServerToken(String token){
		boolean isFull = token.equalsIgnoreCase("Full");
		boolean isProd = token.equalsIgnoreCase("Prod");
		boolean isOff = token.equalsIgnoreCase("Off");
		
		return (isFull || isProd || isOff);
	}
	
	/**
	 * Checks if static.location param is in scope of valid values
	 * @param loc static location
	 * @return check result
	 */
	public static boolean isValidStaticLocation(String loc){
		boolean isOut = loc.equalsIgnoreCase("out");
		boolean isIn = loc.equalsIgnoreCase("in");
		
		return (isOut || isIn);
	}
	
	/**
	 * Static Resource validator 
	 * 
	 * @return true - when all checks success, false -otherwise 
	 */
	public static boolean validateResourceRoot(){
		String rootPath = System.getProperty("static.root","");
		String loc = System.getProperty("static.location");
		int resVal; 
		if(loc.equals("out")){
			
			//try to get file
			File file = new File(rootPath);
			//check on existence and readability  
			if(!file.isDirectory()){ resVal = 404; }
			if(!file.canRead()){ resVal=  403; }
			resVal = 200;
			
			System.setProperty("static.code",""+resVal);
			return (resVal==200);
		} else if(loc.equals("in")) {
			//we cannot validate root of internal resource due to its nature
			resVal = 200;
			System.setProperty("static.code",""+resVal);
			return (resVal==200);
		} else {
			System.setProperty("static.code","500");
			return false;
		}
		
	}
}
