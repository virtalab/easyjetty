package virta.easyjetty.builders;

import org.eclipse.jetty.server.Connector;

import virta.easyjetty.connectors.ConnectorFabric;
import virta.easyjetty.connectors.HttpConnector;
import virta.easyjetty.keepers.ServerConfig;

public class ConnectorBuilder {
	public static Connector build(){
		
		//create default
		HttpConnector conn = (HttpConnector) ConnectorFabric.getConnector();
		//get Values
		int port = ServerConfig.getConfig().getPort();
		String host = ServerConfig.getConfig().getHost();
		
		if(port!=0){
			conn.setPort(port);
		}
		
		if(host!=null || host != ""){
			conn.setHost(host);
		}
		
		return conn;
	}
}
