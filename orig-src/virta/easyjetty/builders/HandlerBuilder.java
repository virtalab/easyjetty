package virta.easyjetty.builders;

import java.util.HashMap;

import javax.naming.ConfigurationException;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virta.easyjetty.handlers.BaseHandler;

/**
 * Describes Structure of Servlet Handler
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.6
 *
 */
public final class HandlerBuilder {
	private static Logger log = Log.getLogger(HandlerBuilder.class);
	
	private String base;
	private String mainPkg;
	private int mode;
	private HashMap<String,String> servlets;
	
	/**
	 * Constructor
	 * 
	 */
	public HandlerBuilder(){
	}
	
	/**
	 * Returns Base (Path from site root for this site)
	 * Note: Internal use in normal case 
	 * @return Base path of this handler
	 */
	public String getBase() {
		return base;
	}
	
	/**
	 * Sets Base (Path from site root for this site)<BR>
	 * Example: your Jet Site located at http://site/app (assume that http://site is served by nginx or other web server), 
	 * then base will "/app"
	 * 
	 * @param base Base to set
	 */
	public void setBase(String base) {
		this.base = base;
	}
	
	
	/**
	 * Returns mainPkg
	 * Note: internal use normally.
	 * 
	 * @return the mainPkg
	 */
	public String getMainPkg() {
		return mainPkg;
	}
	
	/**
	 * Sets main package, where Servlets are located.
	 * Note: Currently we don't support default package
	 * 
	 * @param mainPkg Main Package to set
	 */
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	
	/**
	 * @return mode is Jetty http options
	 */
	public int getMode() {
		return mode;
	}

	/**
	 * @param mode Jetty http options (default is NO_SESSIONS)
	 */
	public void setMode(int mode) {
		this.mode = mode;
	}

	/**
	 * Returns Servets
	 * Note: internal use only
	 * 
	 * @return Servlets HashMap
	 */
	public HashMap<String, String> getServlets() {
		return servlets;
	}
	/**
	 * Adds Servlet to HashMap
	 * 
	 * @param servletClass Class name (at this version only relative name allowed)
	 * @param path Path which this Servlet services
	 */
	public void addServlet(String servletClass,String path){
		//if no HashMap, we set it
		//TODO also we can init HashMap at constructor in object init
		if(servlets==null){
			HashMap<String, String> srvlts = new HashMap<String,String>();
			this.setServlets(srvlts);
		}
		//since now we work with servlets private prop
		this.servlets.put(servletClass, path);
	}
	/**
	 * Builds Handler from Builder Object
	 * @return BaseHandler
	 */
	public BaseHandler build(){
		//init block
		log.info("Building handler...");
		BaseHandler handler = null;	
		//getting creator
		ServletHandlerBuilder builder = new ServletHandlerBuilder();
		
		//check
		log.info("Checking build...");
		try {
			builder.checkHandler(this);
		} catch (ConfigurationException e) {
			log.warn(e.getMessage(), e);
			log.debug(e);
			return null;
		}
		//configure
		log.info("Configuring build...");
		try{
			handler = builder.configureHandler(this);
		}catch(Exception e){
			log.warn(e.getMessage(), e);
			log.debug(e);
			return null;
		}
		return handler;
	}
	
	/**
	 * Sets Servlets
	 * 
	 * @param servlets the servlets to set
	 */
	private void setServlets(HashMap<String, String> servlets) {
		this.servlets = servlets;
	}
}
