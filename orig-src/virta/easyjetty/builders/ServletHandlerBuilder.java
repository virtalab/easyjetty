package virta.easyjetty.builders;

import java.util.HashMap;
import java.util.Iterator;

import javax.naming.ConfigurationException;
import javax.servlet.http.HttpServlet;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virta.easyjetty.handlers.ServletHandler;

public class ServletHandlerBuilder{
	private static Logger log = Log.getLogger(ServletHandlerBuilder.class);
	
	private boolean isHandlerValid;
	
	public ServletHandlerBuilder(){
		//default
		this.isHandlerValid = false;
	}
	
	public void checkHandler(HandlerBuilder handler) throws ConfigurationException {
		//Config checks
				String base = handler.getBase();
				if(base == null || base.equals("")){
					throw new ConfigurationException("Context in config cannot be empty. Use / instead");
				}
				//mainPkg
				String mainPkg = handler.getMainPkg();
				if(mainPkg==null || mainPkg.equals("")){
					log.warn("Main package is not set or empty. Without this I'll search your Servlets at 'default' package. Is that what you want? If no, please add main package using addMainPkg() method");
				}
				//mode
				int mode = handler.getMode();
				if(mode != 0){
				switch (mode) {
				case ServletContextHandler.NO_SECURITY | ServletContextHandler.NO_SESSIONS | ServletContextHandler.SECURITY | ServletContextHandler.SESSIONS:
					break;
				default:
					throw new ConfigurationException("Mode is wrong. See JavaDoc for ServletContextHandler for valid values");
				}
				}
				//Servlets
				HashMap<String, String> servlets = handler.getServlets();
				if(servlets==null){
					throw new ConfigurationException("I cannot start without servlets. Please fix you configuration or start making your first servlet");
				}
				//if we reached here, then handler is valid
				this.isHandlerValid = true;
	}

	public ServletHandler configureHandler(HandlerBuilder hs) throws InstantiationException, IllegalAccessException, ClassNotFoundException, ConfigurationException {
		if(!this.isHandlerValid){
			throw new ConfigurationException("Check was unsuccessful. HandlerStruct is not valid");
		}
		//get values from struct
		String base = hs.getBase();
		String mainPkg = hs.getMainPkg();
		int mode = hs.getMode();
		HashMap<String, String > servlets = hs.getServlets();
		
		ServletHandler handler = null;
		
		//make handler
		if(mode==0){
			handler = new ServletHandler(ServletContextHandler.NO_SESSIONS);
		} else {
			handler = new ServletHandler(mode);
		}
		
		handler.setBase(base);
				
		//adding servlets				
		Iterator<String> iter = servlets.keySet().iterator();
		
		while(iter.hasNext()){
			String servletName = (String) iter.next();
			String path = servlets.get(servletName);
				
			//reflection
			Class<?> srvlt;
			if(mainPkg==null){
				srvlt = Class.forName(servletName);
			} else {
				srvlt = Class.forName(mainPkg+"."+servletName);
			}
            HttpServlet servlet = (HttpServlet) srvlt.newInstance();
	            
            handler.addServlet(servlet,path);
		}
		return handler;
	}

}
