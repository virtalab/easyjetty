package virta.easyjetty.server;

import java.util.ArrayList;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virta.easyjetty.handlers.RootHandler;
import virta.easyjetty.keepers.App;
import virta.easyjetty.keepers.ServerConfig;
import virta.easyjetty.logger.AccessLogger;
import virta.easyjetty.logger.ErrorLogger;
import virta.sys.Shell;

/**
 * Internal Server Mechanisms
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class EasyServer {
	private static final Logger log = Log.getLogger(EasyServer.class);
	/**
	 * Runner New Edition (with iterators config)
	 * 
	 * @throws Exception
	 */
	public static void run() throws Exception{
		//init 
		Server server = new Server();
		
		//getting config
		ServerConfig config = ServerConfig.getConfig();
		ArrayList<Connector> connectors = config.getConnectors();
		//test
		if(connectors ==null || connectors.size()==0){
			throw new NoSuchFieldException("No connectors present. Make sure that all your custom connectors are valid.");
		}
		//toArray
		Connector[] connArr = (Connector[]) connectors.toArray(new Connector[connectors.size()]); 
		server.setConnectors(connArr);
		
		//set handlers
		HandlerCollection handlers = new HandlerCollection();
		
		//init for handlers
		ContextHandler context =null;
		RequestLogHandler accessLogger = null;
		RequestLogHandler errorLogger = null;
		
		//root handler
		RootHandler root = RootHandler.getInstance();
		context = new ContextHandler();
		context.setContextPath(root.getRootBase());
		context.setHandler(root);

		//access log handler
		AccessLogger accessLog = App.getApp().getAccessLog();
		if(accessLog!=null){
			accessLogger = new RequestLogHandler();
			accessLogger.setRequestLog(accessLog);
		}
		//error log handler
		ErrorLogger errorLog = App.getApp().getErrorLog();
		if(errorLog!=null){
			errorLogger = new RequestLogHandler();
			errorLogger.setRequestLog(errorLog);
		}
		
		//add handlers
		ArrayList<Handler> handlersList = new ArrayList<Handler>(); 
		
		if(context!=null){
			log.debug("Adding root handler");
			handlersList.add(context);
		}

		if(accessLogger!=null){
			log.debug("Adding access log handler");
			handlersList.add(accessLogger);
		}
		if(errorLogger!=null){
			log.debug("Adding error log handler");
			handlersList.add(errorLogger);
		}
		
		
		Handler[] handlerArr = handlersList.toArray(new Handler[handlersList.size()]);
		
		handlers.setHandlers(handlerArr);
		
		server.setHandler(handlers);
		//server.setHandler(context);
		
		//run
		server.start();
		if(EasyServer.dump()){Shell.echo(server.dump());}
		server.join();
	}
	private static boolean dump(){
		if(System.getProperty("server.debug")!=null){
			return (System.getProperty("server.debug").equalsIgnoreCase("TRUE"));
		} else {
			return false;
		}
	}
}
