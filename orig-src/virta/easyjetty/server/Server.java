package virta.easyjetty.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virta.easyjetty.builders.ConnectorBuilder;
import virta.easyjetty.builders.HandlerBuilder;
import virta.easyjetty.connectors.ConnectorFabric;
import virta.easyjetty.connectors.HttpConnector;
import virta.easyjetty.connectors.HttpsConnector;
import virta.easyjetty.handlers.BaseHandler;
import virta.easyjetty.handlers.RootHandler;
import virta.easyjetty.keepers.App;
import virta.easyjetty.keepers.ServerConfig;
import virta.easyjetty.util.Init;
import virta.sys.Env;
import virta.sys.Shell;

/**
 * WebJet Server user interface
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class Server {
	 
	private static final Logger log = Log.getLogger(Server.class);
	/**
	 * Provides server object
	 * 
	 * @param args Arguments from command line
	 * @throws ExecutionException when have unrecoverable error during init
	 * @throws IOException when config file not found
	 */ 
	public Server(String[] args) throws ExecutionException, IOException{
		Init.init(args);
	}
	/**
	 * Returns HTTP Connector
	 * @return empty HTTP Connector
	 */
	public HttpConnector getHttpConnector(){
		return (HttpConnector) ConnectorFabric.getConnector();
	}
	/**
	 * Returns HTTPS Connector
	 *
	 * @return empty HTTPS Connector
	 */
	public HttpsConnector getHttpsConnector(){
		return (HttpsConnector) ConnectorFabric.getConnector("ssl");
	}
	
	public void addConnector(Connector conn){
		ServerConfig config = ServerConfig.getConfig();
		config.addConnector(conn);
	}
	
	/**
	 * Checks, configures and adds Handler to server
	 * 
	 * @param handler Ready-to-use handler
	 */
	public void addHandler(BaseHandler handler){
		//init
		RootHandler root = RootHandler.getInstance();
		//FIXME move this inside if to avoid NPE
		String base = handler.getBase();
		//add 
		if(handler!=null){
			log.info("Adding "+handler.getClass().getName() + " to server");
			root.addHandler(handler, base);
		}
	}
	/**
	 * Adds "Pure" (not native for this app) Handler
	 * @param handler
	 */
	public void addPureHandler(Handler handler, String base){
		log.info("Adding "+handler.getClass().getName() + " to server");
		RootHandler.getInstance().addHandler(handler, base);
	}
	
	/**
	 * Returns Handler with default base and no main Package <br>
	 * Note: if you about to use this method make sure that you adding servlets with full name (package + class name),
	 * otherwise we cannot find and use your servlets.   
	 * 
	 * @return Raw Handler (Standard handler yet without Servlets) 
	 */
	public HandlerBuilder getSimpleBuilder(){
		return this.getSimpleBuilder("");
	}
	/**
	 * Returns Handler with default base
	 * 
	 * @param mainPackage Main Package with servlets inside. Note: mixing package this approach (with main package) and full servlet name is not possible
	 * @return Raw Handler (Standard handler yet without Servlets)    
	 */
	public HandlerBuilder getSimpleBuilder(String mainPackage){
		HandlerBuilder handler = new HandlerBuilder();
		handler.setBase("/");
		handler.setMainPkg(mainPackage);
		return handler;
	}
	
	/**
	 * Returns new Handler Builder
	 * 
	 * @return builder object
	 */
	public HandlerBuilder getBuilder(){
		return new HandlerBuilder();
	}
	
	/**
	 * Return new Handler Builder with base
	 * @param base base
	 * @return Handler Builder with already set base
	 */
	public HandlerBuilder getBuilder(String base){
		 HandlerBuilder hb = new HandlerBuilder();
		 hb.setBase(base);
		 return hb;
	}
	
	/**
	 * Allow to set non-standard server base 
	 * @param base
	 */
	public void setRootBase(String base){
		RootHandler.getInstance().setRootBase(base);
	}
	
	
	/**
	 * Triggers server start
	 */
	public void run(){
		//init
		ArrayList<Connector> connectors = ServerConfig.getConfig().getConnectors();
		
		log.info("write pid");
		this.writePid();
		
		if(connectors.size()==0){
			log.info("Custom connector not set. Creating default one.");
			Connector conn =  ConnectorBuilder.build();
			this.addConnector(conn);
		}
		
		try{
			log.info("Starting server");
			EasyServer.run();
		}catch(Exception e){
			log.warn("Server failed to start. Exiting", e);
			log.debug(e);
			//stop JVM
			System.exit(1);
		}
	}
	
	
private void writePid(){
	
	if(App.getApp().getPidFile().equals("")){
		//send output to StdOut
		log.info("Server PID: "+ Env.getMyPid());
	} else {
		String pidFile = App.getApp().getPidFile();
		log.info("Writing pid to "+pidFile);
		
		//Register PID Cleaner as shutdown hook
		PidCleaner cleaner = new PidCleaner();
		Runtime.getRuntime().addShutdownHook(cleaner);
		try{
			int pid = Env.getMyPid();
			if(!pidFile.equals("")){
				//write to file
				Shell.echo(pid,pidFile);
			}	
		}catch(IOException ioe){
			log.warn("I cannot write to PID file. Please use debug mode to find reason");
			log.debug(ioe);
		}
	}
}
//INNER CLASS
private static class PidCleaner extends Thread {
	public void run(){
		String pidFile = App.getApp().getPidFile();
		try {
			Shell.rm(pidFile);
		} catch (IOException e) {
			log.debug(e);
			log.info("Could not remove PID File. Goodbye");
		}
		log.info("PidFile removed. Server shutdown.");
	}
}
}