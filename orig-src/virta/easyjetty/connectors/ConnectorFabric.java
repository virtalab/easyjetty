package virta.easyjetty.connectors;

import org.eclipse.jetty.server.Connector;

public class ConnectorFabric {
	//default connector
	public static Connector getConnector(){
		return getConnector("");
	}
	
	//concrete connector
	public static Connector getConnector(String name){
		if(name.equalsIgnoreCase("ssl")){
			return new HttpsConnector();
		} else {
			return new HttpConnector();
		}
	}
}
