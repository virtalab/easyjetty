package virta.easyjetty.keepers;

import virta.easyjetty.logger.AccessLogger;
import virta.easyjetty.logger.ErrorLogger;
import virta.easyjetty.util.S;
/**
 * App Settings 
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class App {

	private static App self;
	
	
	//object fields
	
	private App(){}
	
	private AccessLogger accessLog = null;
	private ErrorLogger errorLog = null;
	private String pidFile ="";
	
	//defaults
	private String serverToken = "PROD";
	private String serverProduct = S.PRODUCT;
	private String serverVersion = S.VERSION;
	
	private Throwable e;
	
	/**
	 * Object creator
	 * @return App Obejct
	 * 
	 */
	public static App init(){
		if(self==null){
			self = new App();
		}
		return self;
	}
	/**
	 * Return App (Settings objects)
	 * 
	 * @return Settings object (same every time)
	 */
	public static App getApp(){
		return App.init();
	}
	
	/**
	 * Gives access logger
	 * 
	 * @return access logger
	 */
	public AccessLogger getAccessLog() {
		return accessLog;
	}
	
	/**
	 * Sets access.log setting
	 * 
	 * @param accessLog access logger
	 */
	public void setAccessLog(AccessLogger accessLog) {
		this.accessLog = accessLog;
	}
	
	/**
	 * Gives error logger
	 * 
	 * @return error logger
	 */
	public ErrorLogger getErrorLog() {
		return errorLog;
	}
	
	/**
	 * Sets error.logger
	 * 
	 * @param errorLog error logger
	 */
	public void setErrorLog(ErrorLogger errorLog) {
		this.errorLog = errorLog;
	}
	
	/**
	 * Gives pid file name
	 * 
	 * @return pid file name or "shell"
	 */
	public String getPidFile() {
		return pidFile;
	}

	/**
	 * Sets pid file setting
	 * 
	 * @param pidFile file name 
	 */
	public void setPidFile(String pidFile) {
		this.pidFile = pidFile;
	}

	/**
	 * Defines if any error occured at prev. stages or not
	 * 
	 * @return True - when no error was occured, False - elsewhere 
	 */
	public boolean isAppClean() {
		return (e == null);
	}
	/**
	 * Stores Exception (for isAppClean() usage)
	 * 
	 * @param e Exception
	 */
	public void setE(Throwable e) {
		if(e==null) return;
		
		this.e = e;
	}
	
	public String getServerToken() {
		return serverToken;
	}
	public void setServerToken(String serverToken) {
		this.serverToken = serverToken;
	}
	public String getServerProduct() {
		return serverProduct;
	}
	public void setServerProduct(String serverProduct) {
		this.serverProduct = serverProduct;
	}
	public String getServerVersion() {
		return serverVersion;
	}
	public void setServerVersion(String serverVersion) {
		this.serverVersion = serverVersion;
	}
}
