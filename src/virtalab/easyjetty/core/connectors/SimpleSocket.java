package virtalab.easyjetty.core.connectors;

import java.net.SocketException;

import virtalab.easyjetty.app.validation.Validator;
import virtalab.net.DNSWizard;
import virtalab.regexp.RegExps;

/**
 * Struct for keeping TCP Server Socket (ip:port combination)
 *
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
public class SimpleSocket implements Comparable<SimpleSocket> {

	private String ip;
	private int port;


	public SimpleSocket(int port) throws IllegalArgumentException, SocketException{
		this("0.0.0.0",port);
	}

	public SimpleSocket(String ip,int port) throws IllegalArgumentException, SocketException{
		this.setPort(port);
		this.setIp(ip);
	}


	/**
	 * @param port port number
	 * @throws IllegalArgumentException when passed port is not valid
	 * @throws SocketException when port equals 0 (Socket cannot be created, but argument is definetly valid)
	 */
	public void setPort(int port) throws IllegalArgumentException, SocketException{
		if(Validator.isValidPort(port)){
			this.port = port;
		} else {
			if(port!=0){
				throw new IllegalArgumentException("Port "+port+" is not valid. Socket cannot be created.");
			} else {
				throw new SocketException();
			}
		}

	}
	/**
	 * @param ip the ip address as String or hostname
	 * @throws IllegalArgumentException when passed value is not valid
	 */
	public void setIp(String ip) throws IllegalArgumentException {
		if(Validator.isValidHost(ip)){
			this.ip = ip;
		} else {
			throw new IllegalArgumentException("Provided value "+ip+" appears as not valid IP address or hostname that cannot be resolved.");
		}
	}

	/**
	 * @return ip address as String
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @return port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @return Socket (ip:port) as text
	 */
	public String getSocket(){
		return this.ip+":"+this.port;
	}

	/**
	 * Compares two Sockets
	 *
	 * @return 0 if objects are equal, 1 - objects are not equal, -1 - objects cannot br compared
	 */
	@Override
	public int compareTo(SimpleSocket s1) {

		boolean portsAreEqual = (this.port == s1.port);

		if(!portsAreEqual){ return 1; }

		//We don't checking ports below this line, because if ports are not equals this code is already unreachable

		String myIpAddr = null;
		String theyIpAddr = null;

		//hostname to ip. To compare ips
		if(!(RegExps.IPv4(this.ip))){
			//try to resolve

			myIpAddr = DNSWizard.name2ip(this.ip);
		}else {
			myIpAddr = this.ip;
		}

		if(!(RegExps.IPv4(s1.ip))){
			//try to resolve
			theyIpAddr = DNSWizard.name2ip(s1.ip);
		} else {
			//just ip
			theyIpAddr = s1.ip;
		}

		//some name cannot be resolved - it means we cannot compare
		if(myIpAddr==null || theyIpAddr==null){
			return -1;
		}

		boolean ipsAreEqual = (myIpAddr.equals(theyIpAddr));

		//compare IPs
		if(ipsAreEqual){
			return 0;
		}

		//Special comparator code for Wildcard
		String WILDCARD = "0.0.0.0";

		if(myIpAddr.equals(WILDCARD) || theyIpAddr.equals(WILDCARD)){
			//WILDCARD Compapator
			if(myIpAddr.equals(WILDCARD)){
				//they IP is concrete and we are WildCard (all IP) - objects are "equal" (we already have their ip)
				return 0;
			}
			if(theyIpAddr.equals(WILDCARD)){
				//they have WildCard and we have concrete - object are not "equal" (because WildCard includes our IP)
				return 1;
			}
		} else {
			//nobody has wildcard and IPs not equal
			return 1;
		}

		//Possibly Dead code
		return -1;
	}

}
