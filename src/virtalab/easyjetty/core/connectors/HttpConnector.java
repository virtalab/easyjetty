package virtalab.easyjetty.core.connectors;

import org.eclipse.jetty.server.nio.SelectChannelConnector;

/**
 * Provides easy to remember alias to SelectChannelConnector
 *
 * @author Alex Muravya (aka kyberorg) <asm at virtalab.net>
 *
 */
public class HttpConnector extends SelectChannelConnector {
	/**
	 * Constructor disables setting SO_REUSEADDRESS flag to fix pure Jetty behavior, which allows starting 2+ servers at one socket same time
	 */
	public HttpConnector(){
		super();
		super.setReuseAddress(false);
	}
}
