package virtalab.easyjetty.core.servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.sys.Shell;


abstract public class CoreServlet extends HttpServlet {
	private static final long serialVersionUID = -7279265093881718347L;
	private static final Logger log = Log.getLogger(CoreServlet.class);

	/**
	 * Provides static resource as string
	 *
	 * @param req Request object
	 * @param resp Request response
	 * @return resource as String or null (with setting status to response)
	 */
	public static String doStatic(HttpServletRequest req,HttpServletResponse resp){
		//init
		String requestRes = req.getRequestURI();
		String resultStr = null;

		//check if we have valid static root and if not just parse number and reply code
		String code = System.getProperty("static.code","500");
		String rootPath = System.getProperty("static.root","");
		String loc = System.getProperty("static.location");


		//block for failed verification of static.root
		if(!code.equals("200")){
			int intCode;
			try{
				intCode = Integer.parseInt(code);
			}catch(NumberFormatException e){
				//it seems like verification failed
				log.warn("It seems like server is misconfigured (or not configured) for serving static requests. You will always have error 500. See manual for more info");
				log.debug(e);
				resp.setStatus(500);
				return null;
			}
			resp.setStatus(intCode);
			return null;
		}

		//check if requested result is not null and not empty
		 if(requestRes==null || requestRes.equals("")){
			 resp.setStatus(400);
			 return null;
		 }

		 if(loc.equals("out")){
			//external resource
			String path = rootPath + requestRes;
			File file = new File(path);

			if(file==null|| !(file.isFile()) || !(file.exists()) ){
				 //no resource found
				 resp.setStatus(404);
				 return null;
			}

			if(file.isDirectory()){
				//requested resource is directory
				//we cannot stream directory as string, that's why 403 in this version
				resp.setStatus(403);
				return null;
			}

			if(!file.canRead()){
					//cannot read resource - access modifier is wrong
					resp.setStatus(403);
					return null;
			}

			try{
				resultStr = Shell.cat(file);
			}catch(Exception e){
				//in case of some unknown error
				log.debug(e);
				resp.setStatus(500);
				return null;
			}
			//if we alive - return string
			return resultStr;

		 } else if(loc.equals("in") || loc.equals("none")){
			//inner resource
			 String path;
			 if(rootPath.equals("")){
				 path = requestRes;
			 } else {
				 path = rootPath + requestRes;

			 InputStream resStream=CoreServlet.class.getResourceAsStream(path);
			 if(resStream!=null){
				 BufferedReader br = new BufferedReader(new InputStreamReader(resStream));
				 StringBuffer sb = new StringBuffer();
				 String line;
				 try{
				 while((line = br.readLine()) != null){
					 sb.append(line);
				 }
				 resultStr = sb.toString();
				 br.close();
				 }catch(IOException e){
					 log.debug(e);
					 resp.setStatus(500);
					 return null;
				 }
			 } else {
				 //stream not found
				 if(path.endsWith("/")){
					 //directory requested
					 resp.setStatus(403);
					 return null;
				 } else {
					 //file requested
					 resp.setStatus(404);
					 return null;
				 }
			 }
		 }
		}
		//and finally return
		return resultStr;

	}

	/**
	 * Writes response to Client-side (Browser or other receiver) + adds UTF-8 header in reply
	 *
	 * @param body Web Page Content to send
	 * @throws IOException when cannot write to Client-side
	 * @throws ServletException when got not-valid response or don't get get it at all (normally this happens when sendResponse() wasn't called before reply())
	 */
	protected static void reply(String body, HttpServletResponse resp) throws IOException, ServletException{
		if(resp!=null){
			//add header
			resp.setHeader("Content-Type", "text/html; charset=UTF-8");
			//reply
			resp.getWriter().println(body);
		} else {
			throw new ServletException("Response is not set");
		}
	}
}
