package virtalab.easyjetty.core.builders;

import java.util.HashMap;
import java.util.Iterator;

import javax.naming.ConfigurationException;
import javax.servlet.http.HttpServlet;

import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.core.handlers.JettyServletHandler;
import virtalab.easyjetty.core.handlers.ServletHandler;
import virtalab.sys.Shell;

public class HandlerWizard {
	private static Logger log = Log.getLogger(HandlerWizard.class);

	private boolean isHandlerValid;
	private JettyServletHandler handler;

	public HandlerWizard(){
		this.isHandlerValid = false;
	}


	public JettyServletHandler build(ServletHandler sh){
		//check
		log.debug("Checking build...");
		try {
			this.check(sh);
		} catch (ConfigurationException e) {
			log.warn(e.getMessage(), e);
			log.debug(e);
			return null;
		}
		//configure
		log.debug("Configuring build...");
		try{
			this.configure(sh);
		}catch(Exception e){
			log.warn(e.getMessage(), e);
			log.debug(e);
			return null;
		}
		return this.handler;
	}

	private void check(ServletHandler sh) throws ConfigurationException{
		//Config checks
		String base = sh.getBase();
		if(base == null || base.equals("")){
			throw new ConfigurationException("Context in config cannot be empty. Use / instead");
		}
		//mainPkg
		String mainPkg = sh.getMainPkg();
		if(mainPkg==null || mainPkg.equals("")){
			log.warn("Main package is not set or empty. Without this I'll search your Servlets at 'default' package. Is that what you want? If no, please add main package using addMainPkg() method");
		}
		//mode
		int mode = sh.getMode();
		if(mode != 0){
		switch (mode) {
		case ServletContextHandler.NO_SECURITY | ServletContextHandler.NO_SESSIONS | ServletContextHandler.SECURITY | ServletContextHandler.SESSIONS:
			break;
		default:
			throw new ConfigurationException("Mode is wrong. See JavaDoc for ServletContextHandler for valid values");
		}
		}
		//Servlets
		HashMap<String, String> servlets = sh.getServlets();
		if(servlets==null){
			throw new ConfigurationException("I cannot start without servlets. Please fix you configuration or start making your first servlet");
		}
		//if we reached here, then handler is valid
		this.isHandlerValid = true;
	}

	private void configure(ServletHandler sh) throws ConfigurationException, ClassNotFoundException, InstantiationException, IllegalAccessException{
		if(!this.isHandlerValid){
			throw new ConfigurationException("Check was unsuccessful. HandlerStruct is not valid");
		}
		//get values from struct
		String base = sh.getBase();
		String mainPkg = sh.getMainPkg();
		int mode = sh.getMode();
		HashMap<String, String > servlets = sh.getServlets();

		//make handler
		if(mode==0){
			this.handler = new JettyServletHandler(ServletContextHandler.NO_SESSIONS);
		} else {
			this.handler = new JettyServletHandler(mode);
		}

		this.handler.setBase(base);

		//adding servlets
		Iterator<String> iter = servlets.keySet().iterator();

		Shell.echo("Adding Servlets to "+sh.getName());

		while(iter.hasNext()){
			String servletName = iter.next();
			String path = servlets.get(servletName);

			//reflection
			Class<?> srvlt;
			if(mainPkg==null){
				srvlt = Class.forName(servletName);
			} else {
				srvlt = Class.forName(mainPkg+"."+servletName);
			}
            HttpServlet servlet = (HttpServlet) srvlt.newInstance();

            this.handler.addServlet(servlet,path);
            Shell.echo("  "+servletName+" -> "+sh.getName()+" (Relative Path: "+path+")");
            //Shell.echo("Adding "+servletName+" to "+sh.getName()+" (Relative Path: "+path+")");
		}
	}

}
