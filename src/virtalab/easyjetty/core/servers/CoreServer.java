package virtalab.easyjetty.core.servers;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.App;
import virtalab.easyjetty.app.Conf;
import virtalab.easyjetty.app.config.Defaults;
import virtalab.easyjetty.app.configurators.LoggerWizard;
import virtalab.easyjetty.app.exceptions.ConfigurationException;
import virtalab.easyjetty.app.util.LOG;
import virtalab.easyjetty.app.validation.Validator;
import virtalab.easyjetty.core.builders.HandlerWizard;
import virtalab.easyjetty.core.connectors.SimpleSocket;
import virtalab.easyjetty.core.handlers.CoreHandler;
import virtalab.easyjetty.core.handlers.JettyServletHandler;
import virtalab.easyjetty.core.handlers.ServletHandler;
import virtalab.sys.Shell;

abstract public class CoreServer {

	private static final Logger log = Log.getLogger(CoreServer.class);

	protected ArrayList<Connector> connectors = new ArrayList<Connector>();
	protected ArrayList<NCSARequestLog> loggers = new ArrayList<NCSARequestLog>();
	protected HashMap<String,Handler> handlers = new HashMap<String,Handler>();

	private final ArrayList<SimpleSocket> activeBinds = new ArrayList<SimpleSocket>();
	/**
	 * Flag to define, if configuration has error (no connectors or no handlers are present).
	 * If so during run we prevent server from running.
	 * Using this way we protect user from seeing weird Exceptions like BindException
	 */
	private boolean isConfigHasError = false;

	protected void init() throws ConfigurationException{
		App.init();
	}
	protected void setDefaults(){
		System.setProperty("server.debug",Defaults.DEBUG);
		System.setProperty("server.token",Defaults.TOKEN);
		System.setProperty("server.prod",Defaults.PRODUCT);
		System.setProperty("server.version",Defaults.VERSION);
		System.setProperty("static.enabled",Defaults.STATIC_FLAG);

		//Loggers
		this.addLogger(LoggerWizard.setupAccessLogger());
		this.addLogger(LoggerWizard.setupErrorLogger());
	}

	public void setSiteBase(String base){
		Conf.getConf().setServerOffset(base);
	}

	//Methods for filling ArrayLists
	protected void addServletHandler(ServletHandler sh, String base) throws ClassCastException,NullPointerException{
		//trggers build and adds handler to handlers HashMap
		if(sh!=null && base!=null){
			//build
			sh.setBase(base);
			HandlerWizard wizard = new HandlerWizard();
			JettyServletHandler handler = wizard.build(sh);
			//add
			handlers.put(base,handler);
			Shell.echo(LOG.HANDLER+sh.getName() + " -> Server (Path: "+base+")");
		}
	}

	protected void addHandler(Handler handler, String base) throws ClassCastException, NullPointerException{
		//adds handler to handlers HashMap
		if(handler!=null && base!=null){
			handlers.put(base,handler);
			Shell.echo(LOG.HANDLER+handler.getClass().getSimpleName() + " -> Server (Path: "+base+")");
		}
	}

	protected void addLogger(NCSARequestLog logger){
		//Adds to loggers AL
		if(logger!= null){
			String filename = logger.getFilename();
			loggers.add(logger);
			if(filename!=null){
				Shell.echo(LOG.LOGGER+logger.getClass().getSimpleName() + "("+filename+") -> Server");
			} else {
				Shell.echo(LOG.LOGGER+logger.getClass().getSimpleName() + " -> Server");
			}

		}
	}

	protected void addConnector(Connector connector){
		//Add to connectors AL
		if(connector!= null){

			int port = connector.getPort();
			String host = connector.getHost();
			SimpleSocket ss = null;

			try{
				ss = new SimpleSocket(host, port);
			}catch(IllegalArgumentException e){
				log.warn(e.getMessage()+" Skipping this socket ("+host+":"+port+")");
				log.debug(e);
				return;
			}catch(SocketException e){
				//We are here, because we using defaults (port 0)
				//We are not stating at default or random port
				//no warning here - just skipping this socket
				return;
			}

			boolean isSocketValid = Validator.isValidSocket(ss);
			boolean isWildCard = (ss.getIp().equals("0.0.0.0"));

			if(!isSocketValid){
					log.warn("Server could not bind to "+ss.getSocket()+". Maybe another program uses it? Skipping this socket.");
					return;
			}

			//Checking if port and host is already been occuped by another connector

			//wildcard
			if(isWildCard){
				if(activeBinds.size()>0){
					log.warn("You are attempting to mix specific ip address and wildcard (aka any hosts) at same time. Only wildcard will used." +
							" If this is not what you want, please fix your connectors setup.");
					log.warn("Resetting all present Connectors.");
					//clean up
					activeBinds.clear();
					connectors.clear();
				}
			} else {
			//simple host
			for (SimpleSocket socket : activeBinds){
				int comparator = socket.compareTo(ss);
				if(comparator == 0){
					log.warn("This host and port ("+ss.getSocket()+") are already used by another connector. Check your setup.");
					return;
				}
				if(comparator == -1){
					//you should never see it
					log.warn("It looks like you trying to add wierd socket here. This is application error. Report this to author.");
					return;
				}
			}

			}
			//All checks are passed adding socket and connector
 			activeBinds.add(ss);
			Shell.echo(LOG.CONN+connector.getClass().getSimpleName() +" (" + connector.getName() +  ") -> Server");
			connectors.add(connector);
		}
	}

	//Transmit AL and HM out methods
	private void checkConnectors(){
		//checking on defaults and give meanful error
		if(Conf.getConf().getPort()==0){
			this.isConfigHasError = true;
			Shell.eecho(LOG.FATAL+"No valid host and/or port was defined. Check your settings. Unable to start server.");
			return;
		}
		if(this.connectors.size()==0){
			this.isConfigHasError = true;
			Shell.eecho(LOG.FATAL+"No connectors (or sockets) are present");
		}

	}

	private void checkHandlers(){
		if(this.handlers.size()==0){
			this.isConfigHasError = true;
			Shell.eecho(LOG.FATAL+"No handlers are present");
		}
	}

	private void checkLoggers(){
		if(this.loggers.size()==0){
			Shell.eecho(LOG.WARN+"No loggers are present. Logging disabled.");
		}
	}


	public void run() throws Exception{
		//check
		this.checkConnectors();
		this.checkHandlers();
		this.checkLoggers();

		if(this.isConfigHasError){
			log.warn("Server config has unrecoverable errors and therefore cannot be started. See logs above.");
			//prevent futher execution
			return;
		}

		//creating Jetty Server
		Server server = new Server();

		String offset = CoreHandler.getInstance().getRootBase();
		if(!offset.equals("/")){
			Shell.echo("Starting Jetty Server (offset: "+offset+")");
		} else {
			Shell.echo("Starting Jetty Server");
		}


		//adding all handlers to root handler
		Iterator<String> iter = this.handlers.keySet().iterator();

		while(iter.hasNext()){
			String path = iter.next();
			Handler h = this.handlers.get(path);

			log.debug("Registering handler (path: "+path+") to CoreHandler");
			CoreHandler.getInstance().addHandler(h, path);
		}

		//set handlers
		HandlerCollection handlersCollection = new HandlerCollection();

		//init block for handlers
		ContextHandler context =null;

		//root handler
		CoreHandler root = CoreHandler.getInstance();
		context = new ContextHandler();
		context.setContextPath(root.getRootBase());
		context.setHandler(root);

		RequestLogHandler loggerHandler;
		//creating log handlers
		ArrayList<RequestLogHandler> loggerHandlers = new ArrayList<RequestLogHandler>();

		for(NCSARequestLog log: loggers){
			loggerHandler = new RequestLogHandler();
			loggerHandler.setRequestLog(log);
			loggerHandlers.add(loggerHandler);
		}

		//add handlers
		ArrayList<Handler> handlersList = new ArrayList<Handler>();

		if(context!=null){
			log.debug("Adding root handler");
			handlersList.add(context);
		}

		log.debug("Adding log handlers");
		for(RequestLogHandler RLH : loggerHandlers){
			handlersList.add(RLH);
		}


		//AL to Arrays
		Connector[] connArr = connectors.toArray(new Connector[connectors.size()]);
		Handler[] handlerArr = handlersList.toArray(new Handler[handlersList.size()]);
		handlersCollection.setHandlers(handlerArr);

		//adding connectors,handlers
		server.setConnectors(connArr);
		server.setHandler(handlersCollection);

		//trying to be polite
		String mode = Conf.getConf().getQuietMode();
		if(mode.equals("no")|| mode.equals("false")){
			App.sayGoodByeOnExit();
		}

		//run
		server.start();
		if(CoreServer.dump()){Shell.echo(server.dump());}
		server.join();
	}
	private static boolean dump(){
		if(System.getProperty("server.debug")!=null){
			return (System.getProperty("server.debug").equalsIgnoreCase("TRUE"));
		} else {
			return false;
		}
	}
}
