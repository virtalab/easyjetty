package virtalab.easyjetty.core.handlers;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

/**
 * Servlet Handler
 *
 * @author Alex Muravya (aka kyberorg) <asm at virtalab.net>
 *
 */
public class JettyServletHandler extends AbstractHandler {
	private static Logger log = Log.getLogger(JettyServletHandler.class);

	private final HashMap<String, HttpServlet> servlets = new HashMap<String, HttpServlet>();
	private final ArrayList<String> methods = new ArrayList<String>();
	private String base;
	//unused yet
	@SuppressWarnings("unused")
	private final int mode;

	public JettyServletHandler(int mode){
		this.mode=mode;
	}
	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		//init
		HttpServlet servlet;

		log.debug("sh full target "+target);
		String key,newTarget;
		//as we need only part of url, we are about to parse it
		String[] tgt = target.split("/");
		if(tgt.length>1){
			key = "/"+tgt[1];
			log.debug("sh Seraching for key "+key);
			newTarget = target.replaceFirst("/"+tgt[1],"");
		} else {
			key = target;
			newTarget = target;
		}
		if(newTarget.equals("")){
			newTarget = "/";
		}
		log.debug("sh New Target is "+newTarget);
		//END of target parser

		//find key in hashMap
		if(servlets.containsKey(key)){
			servlet = servlets.get(key);
			log.debug("sh Request is served by "+servlet.toString());
			this.service(servlet, request, response);
		} else if(servlets.containsKey("/")) {
			//catch-all servlet
			servlet = servlets.get("/");
			log.debug("sh Request is served by "+servlet.toString());
			this.service(servlet, request, response);
		} else {
			//404
			log.debug("Request Servlet not found here");
			response.setStatus(404);
		}

		//check reply
		if(response.getStatus()!=200){
			ErrorHandler.getHandler().handle(target, baseRequest, request, response);
		}

	}

	public void addServlet(HttpServlet servlet, String path){
		servlets.put(path, servlet);
	}

	public void setBase(String base){
		if(base.contains("/")){
			this.base = base;
		} else {
			log.warn("Cannot set given base. Base must contain at least one /. Using defaults.");
			this.base = "/";
		}

	}
	public String getBase(){
		if(this.base.equals("")){
			return "/";
		} else {
			return this.base;
		}
	}
	/**
	 * Checks if servlet if able to serve request and passes request <p>
	 * or replies with 405 error (method not allowed)
	 *
	 * @param servlet Downstream Servlet
	 * @param request HTTP Request
	 * @param response HTTPResponse
	 * @throws ServletException
	 * @throws IOException
	 */
	private void service(HttpServlet servlet, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		//check if servlet capable for serving this type of requests
		String rMethod = request.getMethod().toUpperCase();
		if(this.isMethodSupported(rMethod, servlet)){
			servlet.service(request, response);
		} else {
			if(rMethod=="OPTIONS"){
				response.setStatus(200);
				response.setHeader("Allow",this.generateAllowHeader());
			} else {
				response.setStatus(405);
				response.setHeader("Allow",this.generateAllowHeader());
			}
		}
	}
	/**
	 * Generates Values for Allow Header
	 *
	 * @return String with list of Allowed methods
	 */
	private String generateAllowHeader(){
		StringBuffer sb = new StringBuffer();
		for(String m: methods){
			sb.append(m+",");
		}
		String s = sb.toString();
		//remove last comma
		if(s.length()>0){
			s = s.substring(0, s.length() -1 );
		}
		return s;
	}

	/**
	 * Checks if method is supported by servlet or not <p>
	 * Calls methodsSupported method
	 *
	 * @param method request method
	 * @return test result
	 */
	private boolean isMethodSupported(String method,HttpServlet servlet){
		 methodsSupported(servlet);
		 return methods.contains(method.toUpperCase());
	}
	/**
	 * Check and stored to methods ArrayList methods what are supported in servlet
	 *
	 * @param servlet Servlet for analysis
	 */
	private void methodsSupported(HttpServlet servlet){
		//clear prev methods
		methods.clear();
		//reflect and fill
		Class<? extends HttpServlet> c = servlet.getClass();
		Method m[] = c.getDeclaredMethods();
		for(int i=0; i<m.length; i++){
			String mName = m[i].getName();
			if(mName.contains("do")){
				String method = mName.replace("do","").toUpperCase();
				methods.add(method);
			}
		}
	}
}
