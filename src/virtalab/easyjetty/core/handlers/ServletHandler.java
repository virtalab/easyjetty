package virtalab.easyjetty.core.handlers;

import java.util.HashMap;

public class ServletHandler {
	private final String name;
	private String base;
	private String mainPkg;
	private int mode;
	private final HashMap<String,String> servlets = new HashMap<String,String>();

	/**
	 * Constructor with name
	 * @param name user-defined name for better logging
	 */
	public ServletHandler(String name){
		this.name = name;
	}
	/**
	 * Default constructor
	 */
	public ServletHandler(){
		this.name = "ServletHandler";
	}

	/**
	 * Returns name
	 * @return handler name
	 */
	public String getName(){
		return name;
	}

	/**
	 * Returns Base (Path from site root for this site)
	 * Note: Internal use in normal case
	 * @return Base path of this handler
	 */
	public String getBase() {
		return base;
	}

	/**
	 * Sets Base (Path from site root for this site)<BR>
	 * Example: your Jet Site located at http://site/app (assume that http://site is served by nginx or other web server),
	 * then base will "/app"
	 *
	 * @param base Base to set
	 */
	public void setBase(String base) {
		this.base = base;
	}


	/**
	 * Returns mainPkg
	 * Note: internal use normally.
	 *
	 * @return the mainPkg
	 */
	public String getMainPkg() {
		return mainPkg;
	}

	/**
	 * Sets main package, where Servlets are located.
	 * Note: Currently we don't support default package
	 *
	 * @param mainPkg Main Package to set
	 */
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}

	/**
	 * @return mode is Jetty http options
	 */
	public int getMode() {
		return mode;
	}

	/**
	 * @param mode Jetty http options (default is NO_SESSIONS)
	 */
	public void setMode(int mode) {
		this.mode = mode;
	}

	/**
	 * Returns Servets
	 * Note: internal use only
	 *
	 * @return Servlets HashMap
	 */
	public HashMap<String, String> getServlets() {
		return servlets;
	}
	/**
	 * Adds Servlet to HashMap
	 *
	 * @param servletClass Class name (at this version only relative name allowed)
	 * @param path Path which this Servlet services
	 */
	public void addServlet(String servletClass,String path){
		//since now we work with servlets private prop
		this.servlets.put(servletClass, path);
	}


}
