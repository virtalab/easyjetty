package virtalab.easyjetty.core.loggers;

import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;

public final class ErrorLogger extends NCSARequestLog {
	public ErrorLogger(){
		super();
	}
	public ErrorLogger(final String filename){
		super(filename);
	}
	
	@Override
	public void log(final Request request, final Response response){
		if(response.getStatus()!=200){
			super.log(request, response);
		}
	}
}
