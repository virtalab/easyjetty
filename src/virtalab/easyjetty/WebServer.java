package virtalab.easyjetty;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.Conf;
import virtalab.easyjetty.app.config.Defaults;
import virtalab.easyjetty.app.exceptions.ConfigurationException;
import virtalab.easyjetty.app.exceptions.NotValidInputException;
import virtalab.easyjetty.app.exceptions.RunException;
import virtalab.easyjetty.app.exceptions.WrongArgumentException;
import virtalab.easyjetty.app.util.Help;
import virtalab.easyjetty.app.validation.InputWizard;
import virtalab.easyjetty.core.connectors.HttpConnector;
import virtalab.easyjetty.core.handlers.ServletHandler;
import virtalab.easyjetty.core.loggers.AccessLogger;
import virtalab.easyjetty.core.loggers.ErrorLogger;
import virtalab.easyjetty.core.servers.CoreServer;

public class WebServer extends CoreServer {

	private static final Logger log = Log.getLogger(WebServer.class);

	//connector
	private final HttpConnector connector = new HttpConnector();

	//handlers
	private ServletHandler handler = new ServletHandler();
	private final HashMap<String, String> servlets = new HashMap<String,String>();


	//Constructors
	public WebServer(String[] args) throws WrongArgumentException, ConfigurationException{
		this.init(args);
	}
	public WebServer() throws ConfigurationException{
		try{
			this.init(null);
		}catch(WrongArgumentException wae){
			log.debug("For this reason: "+wae.getMessage()+" we have an WrongArgumentException. Sorry...");
		}
	}

	public void init(String[] args) throws ConfigurationException, WrongArgumentException{
		//trying to load configuration file or setting defaults
		String conf = System.getProperty(Defaults.CONFIG_PARAM,"");

		if(!conf.equals("")){
			InputWizard.loadConfig(conf);
		} else {
			super.setDefaults();
		}

		//init properties
		InputWizard.PropertiesProccessor();

		if(args!=null){
			//analyze arguments
			InputWizard.ArgumentProccessor(args);

			//If first argument is help flag - showing help and exiting
			if(Help.isHelpFlagDetected(args)){
				Help.showHelp();
				System.exit(0);
			}
		}

		//create Root Handler
		this.handler = new ServletHandler("RootHandler");
		this.handler.setBase("/");

	}

	//Port and Host
	public void setPort(int port) throws ConfigurationException{
		try {
			Conf.getConf().setPort(""+port);
		} catch (NotValidInputException nve) {
			throw new ConfigurationException(nve.getMessage());
		}
	}
	public void setHost(String host) throws ConfigurationException{
		try {
			Conf.getConf().setHost(host);
		} catch (NotValidInputException nve) {
			throw new ConfigurationException(nve.getMessage());
		}
	}

	/**
	 * Adds package where server can find servlets
	 *
	 * @param mainPackage package name as String
	 */
	public void setServletPackage(String mainPackage){
		if(this.handler!=null){
			this.handler.setMainPkg(mainPackage);
		}
	}
	/**
	 * Adds servlet to Server. In case of using custom handlers servlets will be added to latest handler created.
	 *
	 * @param servletClass servlet class name as String
	 * @param base path this servlet maps to
	 */
	public void addServlet(String servletClass,String base){
		servlets.put(servletClass, base);
	}

	public void commitHandler(){
		if(this.handler==null){ return; }
		//adding servlets to handler
		this.prepareServlets();
		//send handler to server
		super.addServletHandler(this.handler,this.handler.getBase());
		//annulate
		this.handler = null;
		this.servlets.clear();
	}

	public void commitRootHandler(){
		this.commitHandler();
	}

	public void createHandler(String name, String base, String mainPackage){
		if(this.handler!=null){
			//warning
			log.warn("You trying to create new handler without commiting previous, this will abandon all changes to previously created handler.");
			log.warn("By default I create Root handler (to serve all incoming requests), if you still want to use this Root handler together with yours: just use commitRootHandler() before creating your handler.");
			this.handler = null;
		}
		//creating new one
		if(name!=null){
			this.handler = new ServletHandler(name);
		} else {
			this.handler = new ServletHandler();
		}

		this.handler.setBase(base);
		if(mainPackage!=null){ this.handler.setMainPkg(mainPackage);}
	}


	@Override
	public void addHandler(Handler handler,String base){
		if(handler != null){
			//two cases: ServletHandler and common Handler
			if(handler instanceof ServletHandler){
				super.addServletHandler((ServletHandler) handler, ((ServletHandler) handler).getBase());
			} else {
				if(base!=null){
					super.addHandler(handler, base);
				} else {
					log.warn("Cannot add Handler: Base cannot be empty");
				}
			}
		}
	}

	@Override
	public void addConnector(Connector connector){
		super.addConnector(connector);
	}

	public AccessLogger getAccessLogger(){
		//trying to get name from config
		String filename = Conf.getConf().getAccessLog();
		if(!filename.equals("")){
			return new AccessLogger(filename);
		} else {
			return new AccessLogger();
		}
	}

	public ErrorLogger getErrorLogger(){
		//trying to get name from config
		String filename = Conf.getConf().getErrorLog();
		if(!filename.equals("")){
			return new ErrorLogger(filename);
		} else {
			return new ErrorLogger();
		}
	}

	@Override
	public void addLogger(NCSARequestLog logger){
		super.addLogger(logger);
	}


	private void prepareServlets(){
		//adding servlets
		Iterator<String> iter = servlets.keySet().iterator();

		while(iter.hasNext()){
			String servletName = iter.next();
			String base = servlets.get(servletName);

			this.handler.addServlet(servletName, base);
		}
	}
	private void makeConnector(){
		//making connector
		connector.setPort(Conf.getConf().getPort());
		connector.setHost(Conf.getConf().getHost());
	}

	@Override
	public void run() throws RunException{
		//checking for any uncommiting handlers
		if(this.handler!=null){
			this.commitHandler();
		}

		//we make connector here to allow user set host and port after init()
		this.makeConnector();
		super.addConnector(connector);

		//running
		try{
			super.run();
		}catch(Exception e){
			log.warn(e.getMessage());
			log.debug(e);
			throw new RunException(e);
		}
	}
}
