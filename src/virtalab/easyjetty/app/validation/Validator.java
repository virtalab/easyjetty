package virtalab.easyjetty.app.validation;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.util.S;
import virtalab.easyjetty.core.connectors.SimpleSocket;
import virtalab.net.DNSWizard;
import virtalab.regexp.RegExps;
import virtalab.sys.Env;

public class Validator {
	private static final Logger log = Log.getLogger(Validator.class);

	/**
	 * Checks if config file exist and readable
	 * @param filename
	 * @return true - when readable, false - elsewhere
	 */
	 public static boolean isValidConfig(String filename){
		File file = new File(filename);
		return (file.isFile() && file.canRead());
	 }
	 /**
	  * String overload for port validator
	  * @param portStr string with port number
	  * @return true if port can be assigned to server, else false
	  */
	 public static boolean isValidPort(String portStr){
		 int port = Integer.parseInt(portStr);
		 return Validator.isValidPort(port);
	 }
	/**
	 * Checks if port is in valid area
	 *
	 * @param port port number
	 * @return true if port can be assigned to server, else false
	 */
	 public static boolean isValidPort(int port){
		 //default
		 if(port == 0){
			 //just return w/o log
		 	return false;
		 }

		if(port < S.MIN_PORT || port > S.MAX_PORT){
			log.warn("Server Port must be a Number from 1 to 65534");
			return false;
		}

		try{
			String OS = Env.getMyOS();
			if(OS!=null){
				if(OS=="unix"){
					if(port <= S.ROOT_PORT){
					//check for root
					String user = System.getProperty("user.name");
					if(!user.equals("root")){
						log.warn("You must be root to start server at ports less then 1024");
						return false;
					}
				}
			}
			}
		}catch(NullPointerException npe){
			log.warn("Your OS cannot be recognized. Skipping root port check.");
		}
		//if we still alive, then all checks passed
		return true;

	}

	/**
	 * Checks if Socket is available to bind
	 *
	 * @param SimpleSocket object
	 * @return true is port can be binded by server, false - otherwise
	 */
	public static boolean isValidSocket(final SimpleSocket ss){
		//checking input
		if(ss==null){ return false; }

		ServerSocket sS = null;
		InetAddress ipAddr = null;
		try{
			ipAddr = InetAddress.getByName(ss.getIp());
			sS = new ServerSocket(ss.getPort(),1,ipAddr);
			return true;
		}catch(final IOException ioe){
			return false;
		} finally {
			if(sS != null){
				try{
					sS.close();
				}catch(IOException ioe){
					//do nothing
				}
			}
		}
	}
	/**
	 * Checks if given hostname or IP are valid for this machine
	 *
	 * @param hostname HostName or IP as String
	 * @return true - if hostname resolves to IP and this IP can be binded to this machine
	 */
	public static boolean isValidHost(String hostname){
		boolean checkRslt = false;
		String ipAddr = null;

		if(!(RegExps.IPv4(hostname))){
			//try to resolve
			ipAddr = DNSWizard.name2ip(hostname);
		} else {
			//already given an IP
			ipAddr = hostname;
		}

		//could not resove
		if(ipAddr == null){
			return false;
		}

		try{
			checkRslt = isValidIpAddr(ipAddr);
		}catch(NumberFormatException e){
			return false;
		}

		return checkRslt;

	}


	/**
	 * Check if IP address can be used at this system
	 *
	 * @param ip string with IP address
	 * @return true - if IP is valid and can be used to bind this server to, false - elsewhere
	 */
	public static boolean isValidIpAddr(String ip){
		//Wildcard is valid
		if(ip.equals("0.0.0.0")){
			return true;
		}

		//Fix for localhost addresses.
		//As only 127.0.0.1 is network interface, 127.0.0.2 and other valid loopbacks cannot be checked in normal way.
		try {
			InetAddress inAddr = InetAddress.getByName(ip);
			if(inAddr.isLoopbackAddress()){
				return true;
			}
		} catch (UnknownHostException e) {
			return false;
		}

		//this is not loopback address - normal check
		//Getting all IP of this machine
		ArrayList<String> ipList = new ArrayList<String>();
		try {
			ipList = Env.getMyIps("ipv4");
		} catch (SocketException e) {
			//we are here if we have no permittions to get interfaces
			log.warn("Cannot check interfaces. Skipping check.");
			return true;
		}

		 for (String ipAddr: ipList){
			 if(ipAddr.equals(ip)){
				 return true;
			 }
		 }
		 //if we are here - no match found, returning false
		 return false;
	 }

	/**
	 * Check if supplied log file Resource is writable, if file don't exist it will be created, if dir is writable
	 *
	 * @param fileName Name of Resource
	 * @return True when file is writable or can be created, false - elsewhere
	 */
	public static boolean isValidLogFile(String fileName){
		File file = new File(fileName);
		if(file.isFile()){
			//file exists, check on write
			return file.canWrite();
		} else {
			//not exists, trying to create
			try {
				file.createNewFile();
			} catch (IOException e) {
				//cannot create
				return false;
			}
		}
		//if we reached this, that means we created file
		return true;
	}

	/**
	 * Checks if PID File exists and writable or can be created
	 *
	 * @param fileName FileName as String
	 * @return True if file is writeable or can be created, false - elsewhere
	 */
	public static boolean isValidPidFile(String fileName){
		File file = new File(fileName);
		if(file.isFile()){
			//file exists, check on write
			return file.canWrite();
		} else {
			//not exists, trying to create
			try {
				file.createNewFile();
			} catch (IOException e) {
				//cannot create
				return false;
			} finally {
				file.delete();
			}
		}
		//if we reached this, that means we created file
		return true;
	}

	/**
	 *Checks if Server token is one of valid values
	 *
	 * @param token Full,Prod or Off header mode
	 * @return check result
	 */
	public static boolean isValidServerToken(String token){
		boolean isFull = token.equalsIgnoreCase("Full");
		boolean isProd = token.equalsIgnoreCase("Prod");
		boolean isOff = token.equalsIgnoreCase("Off");

		return (isFull || isProd || isOff);
	}

	/**
	 * Checks if static.location param is in scope of valid values
	 * @param loc static location
	 * @return check result
	 */
	public static boolean isValidStaticLocation(String loc){
		boolean isOut = loc.equalsIgnoreCase("out");
		boolean isIn = loc.equalsIgnoreCase("in");

		return (isOut || isIn);
	}

	/**
	 * Checks if server.offset is valid offset (contains at least one /)
	 * @param offset server.offset to check
	 * @return true - if offset assumed as valid, false - is not
	 */
	public static boolean isValidOffset(String offset){
		return (offset.contains("/"));
	}

	/**
	 * Static Resource validator
	 *
	 * @return true - when all checks success, false -otherwise
	 */
	public static boolean validateResourceRoot(){
		String rootPath = System.getProperty("static.root","");
		String loc = System.getProperty("static.location");
		int resVal;
		if(loc.equals("out")){

			//try to get file
			File file = new File(rootPath);
			//check on existence and readability
			if(!file.isDirectory()){ resVal = 404; }
			if(!file.canRead()){ resVal=  403; }
			resVal = 200;

			System.setProperty("static.code",""+resVal);
			return (resVal==200);
		} else if(loc.equals("in")) {
			//we cannot validate root of internal resource due to its nature
			resVal = 200;
			System.setProperty("static.code",""+resVal);
			return (resVal==200);
		} else {
			System.setProperty("static.code","500");
			return false;
		}

	}

	/**
	 * Checks if value of quiet mode is ok
	 *
	 * @return true - if matched one of possible valid values, false - elsewhere
	 */
	public static boolean isValidQuietMode(String quietMode){
		boolean isYes = quietMode.equalsIgnoreCase("YES");
		boolean isNo = quietMode.equalsIgnoreCase("NO");
		boolean isTrue = quietMode.equalsIgnoreCase("TRUE");
		boolean isFalse = quietMode.equalsIgnoreCase("FALSE");

		return (isYes || isNo || isTrue || isFalse);

	}
}
