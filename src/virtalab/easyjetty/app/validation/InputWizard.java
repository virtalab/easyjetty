package virtalab.easyjetty.app.validation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Properties;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.Conf;
import virtalab.easyjetty.app.exceptions.ConfigurationException;
import virtalab.easyjetty.app.exceptions.CoreError;
import virtalab.easyjetty.app.exceptions.NotValidInputException;
import virtalab.easyjetty.app.exceptions.WrongArgumentException;
import virtalab.regexp.RegExps;
import virtalab.sys.Shell;

public class InputWizard {

	//static fields
	private static final Logger log = Log.getLogger(InputWizard.class);

	public static void ArgumentProccessor(String[] args) throws WrongArgumentException, ConfigurationException{
		//Analyze of args
		log.debug("Analyzing arguments");

		for(int i=args.length-1;i>=0;i--){
			ArgumentProc(i+1, args[i]);
		}
	}

	private static void ArgumentProc(int seq,String arg) throws WrongArgumentException, ConfigurationException{
		try{
		if(Detector.isIpAddr(arg)){
			setParam(arg,"setHost");
		} else
		if(Detector.isPort(arg)){
			setParam(arg,"setPort");
		} else
		if(Detector.isConfig(arg)){
			loadConfig(arg);
			PropertiesProccessor();
		} else
		if(Detector.isHelp(arg)){
			//just return
			return;
		} else {
			//none of above - assume hostname
			setParam(arg,"setHost");
		}
		}catch(NotValidInputException nve){
			throw new WrongArgumentException(seq, arg);
		}catch(CoreError core){
			throw new WrongArgumentException(core);
		}
	}

	public static void loadConfig(String configName) throws ConfigurationException{
		Prop.loadConfig(configName);
	}

	public static void PropertiesProccessor() throws ConfigurationException{
		try {
			setProperty("server.host","setHost");
			setProperty("server.port", "setPort");
			setProperty("access.log", "setAccessLog");
			setProperty("error.log", "setErrorLog");
			setProperty("server.token", "setServerToken");
			setProperty("server.prod", "setServerProduct");
			setProperty("server.version", "setServerVersion");
			setProperty("server.quiet","setQuietMode");
			setProperty("server.offset","setServerOffset");

			StaticProccessor();

		} catch (NotValidInputException nve) {
			throw new ConfigurationException(nve.getMessage());
		} catch (CoreError core) {
			throw new ConfigurationException(core);
		}

	}

	public static boolean detectHelp(String arg){
		return Detector.isHelp(arg);
	}

	private static void StaticProccessor() throws NotValidInputException, CoreError{
		String staticFlag = System.getProperty("static.enabled","off");
		boolean isStaticEnabled = (staticFlag.equalsIgnoreCase("on"));
		if(isStaticEnabled){
			setProperty("static.location","setStaticLocation");
			setProperty("static.root","setStaticRoot");
		}
	}


	//PRIVATES
	//all dirty job is done here

	private static boolean isPropertyExists(String key){
		//if getProperty returns default Value "", instead of property itself - this means false
		return (!(System.getProperty(key,"").equals("")));
	}

	private static void setProperty(String key,String setter) throws NotValidInputException, CoreError{
		//no key
		if(key==null || setter==null){
			return;
		}
		//key not exists
		if(!isPropertyExists(key)){
			//try to set empty one
			setParam("", setter);
		}
		//key->value
		String value = System.getProperty(key,"");
		if(!(value.equals(""))){
			setParam(value, setter);
		}

	}

	private static void setParam(String value,String setter) throws NotValidInputException,CoreError{

		//String -> Method

		Conf conf = Conf.getConf();
		Method setter0 = null;

		try {
			setter0 = conf.getClass().getMethod(setter, String.class);
		} catch (NoSuchMethodException ex) {
			throw new CoreError("No such method", ex.getCause());
		} catch (SecurityException ex) {
			throw new CoreError("Cannot access method for future invokation", ex.getCause());
		}

		//Invoke
		if(setter0!=null){
			try{
				setter0.invoke(conf, value);
			} catch (IllegalAccessException e) {
				log.debug(e);
				throw new CoreError("Cannot invoke method "+setter0.getName() + " Reason: no access from InputWizard class", e);
			} catch (IllegalArgumentException e) {
				log.debug(e);
				throw new CoreError("Cannot invoke method "+setter0.getName() + " Reason: method cannot be called with arguments like that", e);
			} catch (InvocationTargetException ite) {
				log.debug(ite);
				if(ite.getCause() instanceof NotValidInputException) {
					//exception was thrown my method, that's why we are here
					throw (NotValidInputException)ite.getCause();
				} else {
					//no exception was thrown, just we have exception itself
					throw new CoreError("Cannot invoke method "+ setter0.getName() + "Reason: "+ite.getMessage(), ite);
				}
			}
		}
	}

	//Static Classes

	static class Prop{
		static void loadConfig(String config) throws ConfigurationException{
			if(Validator.isValidConfig(config)){
				//init string for possible exception
				String err = "";
				//try to import values to System.properties
				try {
					importProperties(config);
				} catch (FileNotFoundException e) {
					err = config + ": no such file";
					throw new ConfigurationException("Configuration file is not valid. Reason: "+err);
				} catch (IOException e) {
					err = config + ": cannot read file";
					throw new ConfigurationException("Configuration file is not valid. Reason: "+err);
				}
			} else {
				//not valid config
				throw new ConfigurationException("Configuration file "+config+" is not valid");
			}
		}
		private static void importProperties(String fileName) throws FileNotFoundException, IOException{
			Properties props = new Properties();
			props.load(new FileInputStream(fileName));

			Enumeration<Object> e = props.keys();

			while(e.hasMoreElements()){
				String key = (String) e.nextElement();
				String value = props.getProperty(key).trim();
				//add to System props
				System.setProperty(key, value);
			}


		}
	}//end of Prop

	static class Help{
		//TODO improve help section
		static void showHelp(){
			String help0 = "EasyJetty help";
			String help1 = "Valid options are: ";
			String help2 = "web.config - location of Web Server Configuration file";

			Shell.echo(help0);
			showUsageMessage();
			Shell.echo(help1);
			Shell.echo(help2);
		}

		private static void showUsageMessage(){
			String usage0 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar ";
			String usage1 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar <port> <host>";
			String usage2 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar <host> <port>";

			Shell.echo(usage0);
			Shell.echo(usage1);
			Shell.echo(usage2);
			Shell.echo("");
		}

	}//end of Usage

	static class Detector{

		static boolean isPort(String portStr){
			try{
				Integer.parseInt(portStr);
			}catch(Exception e){
				return false;
			}
			return true;
		}

		static boolean isIpAddr(String ipStr){
			return RegExps.IPv4(ipStr);
		}
		static boolean isHelp(String str){
			return (str.equals("-h") || str.equals("--help"));
		}
		static boolean isConfig(String str){
			return str.endsWith(".conf");
		}
	}

}//end of InputWizard
