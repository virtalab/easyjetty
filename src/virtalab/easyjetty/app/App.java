package virtalab.easyjetty.app;

import virtalab.easyjetty.core.handlers.CoreHandler;
import virtalab.sys.Shell;

public class App {

	public static void init(){
		Conf.init();
		CoreHandler.init();
	}

	public static void sayGoodByeOnExit(){
		GoodByeHook hook = new GoodByeHook();
		Runtime.getRuntime().addShutdownHook(hook);
	}


	//HOOK
	private static class GoodByeHook extends Thread {
		@Override
		public void run(){
			Shell.echo("Web Server shutting down. Bye-bye.");
		}
	}
}
