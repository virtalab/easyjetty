package virtalab.easyjetty.app.configurators;

import virtalab.easyjetty.core.loggers.AccessLogger;
import virtalab.easyjetty.core.loggers.ErrorLogger;


public class LoggerWizard {

/**
 * Setup access logger
 *
 * @param filename Access log filename
 */
public static AccessLogger setupAccessLogger(){
	AccessLogger accessLog = new AccessLogger();
	accessLog.setAppend(true);
	accessLog.setExtended(true);
	accessLog.setRetainDays(0);
	accessLog.setLogTimeZone("UTC");

	return accessLog;
}

/**
 * Setup error logger
 *
 * @param filename Error log filename
 */
public static ErrorLogger setupErrorLogger(){
	ErrorLogger errorLog = new ErrorLogger();
	errorLog.setAppend(true);
	errorLog.setExtended(true);
	errorLog.setRetainDays(0);
	errorLog.setLogTimeZone("UTC");

	return errorLog;
}
}
