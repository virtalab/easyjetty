package virtalab.easyjetty.app.util;

public class LOG {
	/**
	 * Makers for errors
	 */
	public static final String FATAL ="FATAL ";
	public static final String WARN = "WARNING ";

	public static final String LOGGER = "LOG ";
	public static final String HANDLER = "H ";
	public static final String CONN = "CONN ";
}
