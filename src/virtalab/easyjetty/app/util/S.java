package virtalab.easyjetty.app.util;
/**
 * Static constants
 *
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 *
 */
final public class S {
	//prevent init (because Java do not support static classes as C# does)
	private S(){}
	/**
	 * Minimal available port
	 */
	public static final int MIN_PORT=1;
	/**
	 * Maximum available port (as defined in TCP/IP)
	 */
	public static final int MAX_PORT=65535;
	/**
	 * Ports before Root Port cannot be assigned for non-Root user
	 */
	public static final int ROOT_PORT=1024;
	/**
	 * No static value
	 */
	public static final String NO_STATIC_LOCATION="none";
	/**
	 * Static locations
	 */
	public static final String INNER_STATIC_LOCATION="in";
	public static final String OUTTER_STATIC_LOCATION="out";

	/**
	 * Default value of root for inner resourses
	 */
	public static final String DEFAULT_STATIC_ROOT = "/";

}
