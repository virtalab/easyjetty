package virtalab.easyjetty.app.util;

import virtalab.sys.Shell;

public class Help {
	//TODO improve help section
	public static boolean isHelpFlagDetected(String[] args){
		if(args.length>=1){
			return (args[0].equals("-h") || args[0].equals("--help"));
		} else {
			//no args -> no help
			return false;
		}
	}

	public static void showHelp(){
		String lt = System.getProperty("line.separator");

		String help0 = "EasyJetty help";

		StringBuffer arg = new StringBuffer();
		arg.append("Valid Arguments are: "+lt);
		arg.append("config file - Absolute or relative path to conf file. Should end with .conf"+lt);
		arg.append("port -  Valid TCP Port. "+lt);
		arg.append("host - Hostname or IP address. Hostname should be resovleable to ip. IP address should be present at this machine."+lt);
		arg.append("--help | -h - Show this help section.");

		StringBuffer opt = new StringBuffer();
		opt.append("Valid VM options are: "+lt);
		opt.append("web.config - location of Web Server Configuration file"+lt);
		opt.append("virtalab.LEVEL=<log-level> - set required log level"+lt);

		StringBuffer llvl = new StringBuffer();
		llvl.append("Valid log-levels: "+lt);
		llvl.append("DEBUG"+lt);
		llvl.append("INFO"+lt);
		llvl.append("WARN"+lt);

		Shell.echo(help0);
		Shell.echo("");
		showUsageMessage();
		Shell.echo("");
		Shell.echo(arg.toString());
		Shell.echo("");
		Shell.echo(opt.toString());
		//Shell.echo("");
		Shell.echo(llvl.toString());

	}

	private static void showUsageMessage(){
		String usage0 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar ";
		String usage1 = "Usage: java -D<valid-option>=<value> -jar <easyjetty-app>.jar <arguments>";

		Shell.echo(usage0);
		Shell.echo(usage1);
	}

}
