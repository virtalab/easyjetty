package virtalab.easyjetty.app.exceptions;

public class WrongArgumentException extends WebServerException {

	private static final long serialVersionUID = 772471081063920676L;

	public WrongArgumentException(){
		super();
	}

	/**
	 * Creates Exception with message
	 * @param seq argument sequence
	 * @param value argument value
	 */
	public WrongArgumentException(int seq,String value){
		super("Argument "+seq+": \""+value+"\" not valid.");
	}

	public WrongArgumentException(Throwable e){
		super(e);
	}
	public WrongArgumentException(String message,int seq, String value){
		super(message+" Wrong argument is no "+seq+": \""+value+"\"");
	}
}
