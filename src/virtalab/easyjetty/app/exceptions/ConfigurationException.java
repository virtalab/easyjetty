package virtalab.easyjetty.app.exceptions;

public class ConfigurationException extends WebServerException {
	private static final long serialVersionUID = 3940550621688674146L;

	public ConfigurationException(String message){
		super(message);
	}
	public ConfigurationException(Throwable e){
		super(e);
	}

	public ConfigurationException(){
		super();
	}
}
