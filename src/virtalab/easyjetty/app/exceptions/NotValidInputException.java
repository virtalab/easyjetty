package virtalab.easyjetty.app.exceptions;

public class NotValidInputException extends WebServerException {

	private static final long serialVersionUID = 1L;

	public NotValidInputException(){
		super();
	}

	public NotValidInputException(String message){
		super(message);
	}
}
