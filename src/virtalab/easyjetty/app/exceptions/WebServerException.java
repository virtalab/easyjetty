package virtalab.easyjetty.app.exceptions;

public class WebServerException extends Exception {

	private static final long serialVersionUID = -3800092920089240860L;

	public WebServerException(){
		super();
	}

	public WebServerException(Throwable e){
		super(e);
	}

	public WebServerException(String message){
		super(message);
	}
}
