package virtalab.easyjetty.app;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import virtalab.easyjetty.app.config.Defaults;
import virtalab.easyjetty.app.exceptions.NotValidInputException;
import virtalab.easyjetty.app.util.S;
import virtalab.easyjetty.app.validation.Validator;

public class Conf {
	private static final Logger log = Log.getLogger(Conf.class);
	private static Conf self;

	//default host and port
	private String host = Defaults.HOST;
	private int port = Defaults.PORT;
	private String accessLog = "";
	private String errorLog = "";

	private String token = Defaults.TOKEN;
	private String prod = Defaults.PRODUCT;
	private String version = Defaults.VERSION;

	private String staticLoc = S.NO_STATIC_LOCATION;
	private String staticRoot = "";

	private String quietMode = Defaults.QUIET_MODE;

	private String offset = Defaults.SERVER_OFFSET;
	//Singleton init
	private Conf(){}
	public static Conf init(){
		if(self == null){
			self = new Conf();
		}
		return self;
	}
	//alias
	public static Conf getConf(){
		return Conf.init();
	}

	//Setters
	public void setPort(String port) throws NotValidInputException {
		if(port.isEmpty()){return;}

		int intPort;
		try{
			intPort = Integer.parseInt(port);
		}catch(NumberFormatException e){
			throw new NotValidInputException("Cannot parse port "+port+" value as int");
		}

		if(Validator.isValidPort(intPort)){
			this.port = intPort;
		} else {
			throw new NotValidInputException("Port "+port+" is not valid.");
		}


	}
	public void setHost(String host) throws NotValidInputException {
		if(host.isEmpty()){ return; }

		if(Validator.isValidHost(host)){
			this.host = host;
		} else {
			throw new NotValidInputException("Host "+host+ " is not valid.");
		}

	}
	public void setAccessLog(String logName) throws NotValidInputException{
		if(logName.isEmpty()){ return; }

		if(Validator.isValidLogFile(logName)){
			this.accessLog = logName;
		} else {
			throw new NotValidInputException("Access log \""+logName +"\" is not valid");
		}

	}
	public void setErrorLog(String logName) throws NotValidInputException{
		if(logName.isEmpty()){ return; }

		if(Validator.isValidLogFile(logName)){
			this.errorLog = logName;
		} else {
			throw new NotValidInputException("Error log \""+logName +"\" is not valid");
		}
	}
	public void setServerToken(String token) {

		if(Validator.isValidServerToken(token)){
			this.token  = token;
		} else {
			log.warn("Server Token \""+token+"\" is not valid. Using default: "+Defaults.TOKEN+".");
			this.token = Defaults.TOKEN;
		}
	}
	public void setServerProduct(String prod) {
		if(prod.isEmpty()){
			this.prod = Defaults.PRODUCT;
		} else {
			this.prod = prod;
		}
	}
	public void setServerVersion(String version) {
		if(version.isEmpty()){
			this.prod = Defaults.VERSION;
		} else {
			this.version = version;
		}
	}
	public void setStaticLocation(String staticLoc) {
		if(staticLoc.isEmpty()){
			this.staticLoc = S.NO_STATIC_LOCATION;
		}
		if(Validator.isValidStaticLocation(staticLoc)){
			this.staticLoc = staticLoc;
		} else {
			log.warn("Value \""+staticLoc+"\" is Not valid static location: only \""+S.INNER_STATIC_LOCATION+"\" or \""+S.OUTTER_STATIC_LOCATION+"\" are valid. Using default: "+S.INNER_STATIC_LOCATION);
			//setting default
			this.staticLoc = S.INNER_STATIC_LOCATION;
		}
	}
	public void setStaticRoot(String staticRoot) {
		if(staticRoot.isEmpty()){
			//static root not set and static location is set - we warn user about it
			if(!this.staticLoc.equals(S.NO_STATIC_LOCATION)){
				log.warn("static.root is not set. So you cannot use static resources. You will always get error 500.");
			}
			//if static location is inner resources, we set default value
			if(this.staticLoc.equals(S.INNER_STATIC_LOCATION)){
				this.staticRoot = S.DEFAULT_STATIC_ROOT;
			}
		}
	}
	public void setQuietMode(String quietMode) {
		if(quietMode.isEmpty()){ this.quietMode = Defaults.QUIET_MODE; return; }

		if(Validator.isValidQuietMode(quietMode)){
			this.quietMode = quietMode.toLowerCase();
		} else {
			this.quietMode = Defaults.QUIET_MODE;
		}

	}
	public void setServerOffset(String offset){
		if(offset.isEmpty()){ this.offset = Defaults.SERVER_OFFSET; return; }
		if(Validator.isValidOffset(offset)){
			this.offset = offset;
		} else {
			//default
			this.offset = Defaults.SERVER_OFFSET;
			log.warn("WARN cannot set given offset ("+offset+"). Offset must contain at least one /. Using default.");
		}

	}

	//Getters are public
	public int getPort() {
		return this.port;
	}

	public String getHost() {
		return this.host;
	}
	public String getAccessLog() {
		return accessLog;
	}
	public String getErrorLog() {
		return errorLog;
	}
	public String getServerToken() {
		return token;
	}
	public String getServerProduct() {
		return prod;
	}
	public String getServerVersion() {
		return version;
	}
	public String getStaticLocation() {
		return staticLoc;
	}
	public String getStaticRoot() {
		return staticRoot;
	}
	public String getQuietMode() {
		return quietMode;
	}
	public String getServerOffset() {
		return offset;
	}



}
