package virtalab.easyjetty.app.config;

public class Defaults {

	/**
	 * Default JVM Parameter name for Configuration file
	 */
	public static final String CONFIG_PARAM = "web.config";
	/**
	 * Server Port
	 */
	public static final int PORT = 0;
	/**
	 * Server Host
	 */
	public static final String HOST ="0.0.0.0";
	/**
	 * Debug Flag
	 */
	public static final String DEBUG = "false";
	/**
	 * Default Server Token
	 */
	public static final String TOKEN = "PROD";
	/**
	 * Version
	 */
	public static final String VERSION="1.0";
	/**
	 * Product
	 */
	public static final String PRODUCT="Easyjetty";
	/**
	 * Static flag
	 */
	public static final String STATIC_FLAG = "off";
	/**
	 * Quiet Mode
	 */
	public static final String QUIET_MODE = "no";
	/**
	 *
	 */
	public static final String SERVER_OFFSET ="/";
}
